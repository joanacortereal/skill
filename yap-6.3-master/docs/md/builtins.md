YAP Built-ins					{#builtins}
=================

This chapter describes the core predicates  that control the execution of
Prolog programs, provide fundamental functionality such as termm manipulation or arithmetic, and support interaction with external
resources, Many of the predicates described here have been standardised by the ISO. The standartised subset of Proloh also known as ISO-Prolog.                                                                                                                                                                                                     

In the description of the arguments of functors the following notation
will be used:

+ a preceding plus sign will denote an argument as an "input
argument" - it cannot be a free variable at the time of the call;
+ a preceding minus sign will denote an "output argument";
+ an argument with no preceding symbol can be used in both ways.
+ @ref YAPControl

+ @ref Arithmetic

+ @ref YAPChars

+ @ref YAP_Terms

+ @ref InputOutput

+ @ref AbsoluteFileName

+ @ref YAPOS

+ @ref Internal_Database

+ @ref Sets
