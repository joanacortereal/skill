
Downloading YAP           {#download}
==============

The latest development version of Yap-6 is yap-6.3.4 and can be
obtained from the repositories

<http://sourceforge.net/p/yap/yap-6.3>

and

<https://github.com/vscosta/yap-6.3>

YAP-6.3.4 does not use modules. Please just use `git clone` to obtain the distribution.

Most of these repositories are basically copies of the original
repositories at the SWI-Prolog site. YAP-6 will work either with or
without these packages.
