YAP packages files {#packages}
===================

+ @subpage real

+ @ref BDDs

+ @subpage  gecode

+ @subpage  myddas

+ @ref PFL/CLP(BN)

+ @ref ProbLog1

+ @ref Python

+ @subpage YAPRaptor

+ @ref YAP-LBFGS

+ @subpage yap-udi-indexers

Leuven packages ported from SWI-Prolog:

+ @subpage chr

+ @subpage clpqr
