Loading and Oganising YAP Programs           {#consult}
===================================

  Next, we present the main predicates and directives available to load
  files and to control the Prolog environment.

  + @subpage YAPModules

  + @ref YAPConsulting

 +  @ref YAPSaving
