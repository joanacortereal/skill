The YAP Library {#library}
==============

  Library files reside in the library_directory path (set by the
  `LIBDIR` variable in the Makefile for YAP). Several files in the
  library are originally from the public-domain Edinburgh Prolog library.

- @ref apply
- @ref apply_macros
- @ref arg
- @ref Association_Lists
- @ref avl
- @ref bhash
- @ref block_diagram
- @ref c_alarms
- @ref charsio
- @ref clauses
- @ref cleanup
- @ref dbqueues
- @ref dbusage
- @ref dgraphs
- @ref exo_interval
- @ref flags
- @ref gensym
- @ref yap_hacks
- @ref heaps
- @ref lam_mpi
- @ref line_utils
- @ref swi_listing
- @ref lists
- @ref mapargs
- @ref maplist
- @ref matlab
- @ref matrix
- @ref nb
- @ref Ordered_Sets
- @ref parameters
- @ref queues
- @ref random
- @ref Pseudo_Random
- @ref rbtrees
- @ref regexp
- @ref rltrees
- @ref Splay_Trees
- @ref operating_system_support,
- @ref Terms
- @ref timeout
- @ref trees
- @ref tries
- @ref ugraphs
- @ref undgraphs
- @ref varnumbers
- @ref wdgraphs
- @ref wdgraphs
- @ref wdgraphs
- @ref wgraphs
- @ref wundgraphs
- @ref ypp
