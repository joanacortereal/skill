
#ifndef YAPT_HH
#define YAPT_HH 1

#include "config.h"

extern "C" {
Term YAP_ReadBuffer(const char *s, Term *tp);
#if defined(SWIGPYTHON) && 0
#include <Python.h>
extern Term pythonToYAP(PyObject *inp);
#define YAPTerm _YAPTERM
#elifndef HAVE_PYTHON_H
typdef struct { int no_python; } PyObject;
#else
#include <Python.h>                                                                                
#endif
};

class YAPError;

/**
 * @brief Generic Prolog Term
 */
class YAPTerm {
  friend class YAPPredicate;
  friend class YAPPrologPredicate;
  friend class YAPQuery;
  friend class YAPModule;
  friend class YAPModuleProp;
  friend class YAPApplTerm;
  friend class YAPListTerm;

protected:
  yhandle_t t; /// handle to term, equivalent to term_t
public:
  virtual ~YAPTerm() {
    //  fprintf(stderr,"-%d,%lx,%p ",t,LOCAL_HandleBase[t] ,HR);
    //  Yap_DebugPlWriteln(LOCAL_HandleBase[t]); }
    //    LOCAL_HandleBase[t] = TermFreeTerm;
    // while (  LOCAL_HandleBase[LOCAL_CurSlot-1] == TermFreeTerm)
    LOCAL_CurSlot--;
  };

  Term gt() {
    CACHE_REGS
    // fprintf(stderr,"?%d,%lx,%p\n",t,LOCAL_HandleBase[t], HR);
    // Yap_DebugPlWriteln(LOCAL_HandleBase[t]);
    return Yap_GetFromSlot(t);
  };

  void mk(Term t0) {
    CACHE_REGS t = Yap_InitSlot(t0);
    // fprintf(stderr,"+%d,%lx,%p,%p",t,t0,HR,ASP); Yap_DebugPlWriteln(t0);
  };

  YAPTerm(Term tn) { mk(tn); };
  YAPTerm(PyObject *inp) {
#ifdef SWIGPYTHON
    Term tinp = pythonToYAP(inp);
    t = Yap_InitSlot(tinp);
#else
    t = 0;
#endif
  }
  /// private method to convert from Term (internal YAP representation) to
  /// YAPTerm
  // do nothing constructor
  YAPTerm() { mk(MkVarTerm()); };
  // YAPTerm(yhandle_t i) { t = i; };
  /// pointer to term
  YAPTerm(void *ptr);
  /// parse string s and construct a term.
  YAPTerm(char *s) {
    Term tp;
    mk(YAP_ReadBuffer(s, &tp));
  }
  /// construct a term out of an integer (if you know object type use
  /// YAPIntegerTerm)
  /// YAPTerm(long int num) { mk(MkIntegerTerm(num)); }
  /// construct a term out of an integer (if you know object type use
  /// YAPIntegerTerm)
  /// YAPTerm(double num) { mk(MkFloatTerm(num)); }
  /// parse string s and construct a term.
  /// YAPTerm(YAPFunctor f, YAPTerm ts[]);
  /// extract the tag of a term, after dereferencing.
  YAP_tag_t tag();
  /// copy the term ( term copy )
  Term deepCopy();
  /// numbervars ( int start, bool process=false )
  intptr_t numberVars(intptr_t start, bool skip_singletons = false);
  inline Term term() {
    return gt();
  } /// from YAPTerm to Term (internal YAP representation)
  inline void bind(Term b) { LOCAL_HandleBase[t] = b; }
  inline void bind(YAPTerm *b) { LOCAL_HandleBase[t] = b->term(); }
  /// from YAPTerm to Term (internal YAP representation)
  /// fetch a sub-term
  Term &operator[](size_t n);
  // const YAPTerm *vars();
  /// this term is == to t1
  virtual bool exactlyEqual(YAPTerm t1) {
    bool out;
    BACKUP_MACHINE_REGS();
    out = Yap_eq(gt(), t1.term());
    RECOVER_MACHINE_REGS();
    return out;
  };

  /// t = t1
  virtual bool unify(YAPTerm t1) {
    intptr_t out;
    BACKUP_MACHINE_REGS();
    out = Yap_unify(gt(), t1.term());
    RECOVER_MACHINE_REGS();
    return out;
  };

  /// we can unify t and t1
  virtual bool unifiable(YAPTerm t1) {
    bool out;
    BACKUP_MACHINE_REGS();
    out = Yap_eq(gt(), t1.term());
    RECOVER_MACHINE_REGS();
    return out;
  };

  /// t =@= t1, the two terms are equal up to variable renamingvirtual bool
  /// variant(
  inline virtual YAP_Term variant(YAPTerm t1) {
    intptr_t out;
    BACKUP_MACHINE_REGS();
    out = Yap_Variant(gt(), t1.term());
    RECOVER_MACHINE_REGS();
    return out;
  };

  virtual intptr_t hashTerm(size_t sz, size_t depth, bool variant) {
    intptr_t out;

    BACKUP_MACHINE_REGS();
    out = Yap_TermHash(gt(), sz, depth, variant);
    RECOVER_MACHINE_REGS();
    return out;
  };
  /// term hash,
  virtual bool isVar() { return IsVarTerm(gt()); }   /// type check for unound
  virtual bool isAtom() { return IsAtomTerm(gt()); } ///  type check for atom
  virtual bool isInteger() {
    return IsIntegerTerm(gt());
  } /// type check for integer
  virtual bool isFloat() {
    return IsFloatTerm(gt());
  } /// type check for floating-point
  virtual bool isString() {
    return IsStringTerm(gt());
  } /// type check for a string " ... "
  virtual bool isCompound() {
    return !(IsVarTerm(gt()) || IsNumTerm(gt()));
  }                                                  /// is a primitive term
  virtual bool isAppl() { return IsApplTerm(gt()); } /// is a structured term
  virtual bool isPair() { return IsPairTerm(gt()); } /// is a pair term
  virtual bool isGround() { return Yap_IsGroundTerm(gt()); } /// term is ground
  virtual bool isList() { return Yap_IsListTerm(gt()); }     /// term is a list

  /// extract the argument i of the term, where i in 1...arity
  virtual Term getArg(arity_t i) {
    BACKUP_MACHINE_REGS();
    Term tf = 0;
    Term t0 = gt();
    if (IsApplTerm(t0))
      tf = (ArgOfTerm(i, t0));
    else if (IsPairTerm(t0)) {
      if (i == 1)
        tf = (HeadOfTerm(t0));
      else if (i == 2)
        tf = (TailOfTerm(t0));
    } else {
      tf = ((Term)0);
    }
    RECOVER_MACHINE_REGS();
    return tf;
  }

  /// extract the arity of the term
  /// variables have arity 0
  virtual inline arity_t arity() {
    Term t0 = gt();

    if (IsApplTerm(t0)) {
      Functor f = FunctorOfTerm(t0);
      if (IsExtensionFunctor(f))
        return 0;
      return ArityOfFunctor(f);
    } else if (IsPairTerm(t0)) {
      return 2;
    }
    return 0;
  }

  /// return a string with a textual representation of the term
  virtual const char *text(){
  CACHE_REGS
 size_t length = 0;
 encoding_t enc = LOCAL_encoding;
 char *os;

 BACKUP_MACHINE_REGS();
 if (!(os = Yap_TermToString(Yap_GetFromSlot(t), &length, enc,
                             Handle_vars_f))) {
   RECOVER_MACHINE_REGS();
   return 0;
 }
 RECOVER_MACHINE_REGS();
 length = strlen(os) + 1;
 char *sm = (char *)malloc(length + 1);
 strcpy(sm, os);
 return sm;
};

  /// return a handle to the term
  inline yhandle_t handle() { return t; };

  /// whether the term actually refers to a live object
  inline bool initialized() { return t != 0; };
};

/**
 * @brief Variable Term
 */
class YAPVarTerm : public YAPTerm {
  YAPVarTerm(Term t) {
    if (IsVarTerm(t)) {
      mk(t);
    }
  }

public:
  /// constructor
  YAPVarTerm();
  /// get the internal representation
  CELL *getVar() { return VarOfTerm(gt()); }
  /// is the variable bound to another one
  bool unbound() { return IsUnboundVar(VarOfTerm(gt())); }
  virtual bool isVar() { return true; }      /// type check for unbound
  virtual bool isAtom() { return false; }    ///  type check for atom
  virtual bool isInteger() { return false; } /// type check for integer
  virtual bool isFloat() { return false; }   /// type check for floating-point
  virtual bool isString() { return false; }  /// type check for a string " ... "
  virtual bool isCompound() { return false; } /// is a primitive term
  virtual bool isAppl() { return false; }     /// is a structured term
  virtual bool isPair() { return false; }     /// is a pair term
  virtual bool isGround() { return false; }   /// term is ground
  virtual bool isList() { return false; }     /// term is a list
};

/**
 * @brief Compound Term
 */
class YAPApplTerm : public YAPTerm {
  friend class YAPTerm;
  YAPApplTerm(Term t0) { mk(t0); }

public:
  ~YAPApplTerm() {}
   YAPApplTerm(YAPFunctor f, YAPTerm ts[]);
  //YAPApplTerm(const char *s, std::vector<YAPTerm> ts);
  //YAPApplTerm(YAPFunctor f);
  YAPFunctor getFunctor();
  Term getArg(arity_t i) {
    BACKUP_MACHINE_REGS();
    Term t0 = gt();
    Term tf;
    tf = ArgOfTerm(i, t0);
    RECOVER_MACHINE_REGS();
    return tf;
  };
  virtual bool isVar() { return false; }     /// type check for unbound
  virtual bool isAtom() { return false; }    ///  type check for atom
  virtual bool isInteger() { return false; } /// type check for integer
  virtual bool isFloat() { return false; }   /// type check for floating-point
  virtual bool isString() { return false; }  /// type check for a string " ... "
  virtual bool isCompound() { return true; } /// is a primitive term
  virtual bool isAppl() { return true; }     /// is a structured term
  virtual bool isPair() { return false; }    /// is a pair term
  virtual bool isGround() { return true; }   /// term is ground
  virtual bool isList() { return false; }    /// [] is a list
};

/**
 * @brief List Constructor Term
 */
class YAPPairTerm : public YAPTerm {
  friend class YAPTerm;
  YAPPairTerm(Term t0) {
    if (IsPairTerm(t0))
      mk(t0);
    else
      mk(0);
  }

public:
  YAPPairTerm(YAPTerm hd, YAPTerm tl);
  YAPPairTerm();
  Term getHead() { return (HeadOfTerm(gt())); }
  Term getTail() { return (TailOfTerm(gt())); }
};

/**
 * @brief Number Term
 */

class YAPNumberTerm : public YAPTerm {
public:
  YAPNumberTerm(){};
  bool isTagged() { return IsIntTerm(gt()); }
};

/**
 * @brief Integer Term
 */

class YAPIntegerTerm : public YAPNumberTerm {
public:
  YAPIntegerTerm(intptr_t i);
  intptr_t getInteger() { return IntegerOfTerm(gt()); };
};

/**
  * @brief Floating Point Term
  */

class YAPFloatTerm : public YAPNumberTerm {
public:
  YAPFloatTerm(double dbl) { mk(MkFloatTerm(dbl)); };

  double getFl() { return FloatOfTerm(gt()); };
};

class YAPListTerm : public YAPTerm {
public:
  /// Create a list term out of a standard term. Check if a valid operation.
  ///
  /// @param[in] the term
  YAPListTerm() { mk(TermNil); /* else type_error */ }
  /// Create an empty list term.
  ///
  /// @param[in] the term
  YAPListTerm(Term t0) { mk(t0); /* else type_error */ }
  /// Create a list term out of an array of terms.
  ///
  /// @param[in] the array of terms
  /// @param[in] the length of the array
  YAPListTerm(YAPTerm ts[], size_t n);
  //      YAPListTerm( vector<YAPTerm> v );
  /// Return the number of elements in a list term.
  size_t length() {
    Term *tailp;
    Term t1 = gt();
    return Yap_SkipList(&t1, &tailp);
  }
  /// Extract the nth element.
  Term &operator[](size_t n);
  /// Extract the first element of a list.
  ///
  /// @param[in] the list
  Term car();
  /// Extract the tail elements of a list.
  ///
  /// @param[in] the list
  Term cdr() {
    Term to = gt();
    if (IsPairTerm(to))
      return (TailOfTerm(to));
    else if (to == TermNil)
      return TermNil;
    /* error */
    Yap_Error(TYPE_ERROR_LIST, t, 0);
    throw YAPError();
  }
  /// copy a list.
  ///
  /// @param[in] the list
  Term dup();

  /// Check if the list is empty.
  ///
  /// @param[in] the list
  inline bool nil() {
    return gt() == TermNil;
  }

  ;
};

/**
 * @brief String Term
 */
class YAPStringTerm : public YAPTerm {
public:
  /// your standard constructor
  YAPStringTerm(char *s);
  /// use this one to construct length limited strings
  YAPStringTerm(char *s, size_t len);
  /// construct using wide chars
  YAPStringTerm(wchar_t *s);
  /// construct using length-limited wide chars
  YAPStringTerm(wchar_t *s, size_t len);
  const char *getString() { return StringOfTerm(gt()); }
};

/**
 * @brief Atom Term
 * Term Representation of an Atom
 */
class YAPAtomTerm : public YAPTerm {
  friend class YAPModule;
  // Constructor: receives a C-atom;
  YAPAtomTerm(Atom a) { mk(MkAtomTerm(a)); }
  YAPAtomTerm(Term t) : YAPTerm(t) { IsAtomTerm(t); }

public:
  // Constructor: receives an atom;
  YAPAtomTerm(YAPAtom a) : YAPTerm() { mk(MkAtomTerm(a.a)); }
  // Constructor: receives a sequence of ISO-LATIN1 codes;
  YAPAtomTerm(char s[]);
  // Constructor: receives a sequence of up to n ISO-LATIN1 codes;
  YAPAtomTerm(char *s, size_t len);
  // Constructor: receives a sequence of wchar_ts, whatever they may be;
  YAPAtomTerm(wchar_t *s);
  // Constructor: receives a sequence of n wchar_ts, whatever they may be;
  YAPAtomTerm(wchar_t *s, size_t len);
  virtual bool isVar() { return false; }     /// type check for unbound
  virtual bool isAtom() { return true; }     ///  type check for atom
  virtual bool isInteger() { return false; } /// type check for integer
  virtual bool isFloat() { return false; }   /// type check for floating-point
  virtual bool isString() { return false; }  /// type check for a string " ... "
  virtual bool isCompound() { return false; }       /// is a primitive term
  virtual bool isAppl() { return false; }           /// is a structured term
  virtual bool isPair() { return false; }           /// is a pair term
  virtual bool isGround() { return true; }          /// term is ground
  virtual bool isList() { return gt() == TermNil; } /// [] is a list
  // Getter: outputs the atom;
  YAPAtom getAtom() { return YAPAtom(AtomOfTerm(gt())); }
  // Getter: outputs the name as a sequence of ISO-LATIN1 codes;
  const char *text() { return (const char *)AtomOfTerm(gt())->StrOfAE; }
};
#endif /* YAPT_HH */
