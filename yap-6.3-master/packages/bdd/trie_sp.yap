
:- module(trie_sp, [trie_to_bdd/3,
	tabled_trie_to_bdd/3]).

:- use_module((bdd)).
:- use_module(library(tries)).
:- use_module(library(rbtrees)).
:- use_module(library(lists)).

trie_to_bdd(Trie, BDD, MapList) :-
	%trie_print(Trie),
	trie_to_list(Trie, Complex),
	%writeln(Complex),
	%(numbervars(Complex,1,_), writeln(Complex), fail ; true ),
	rb_new(Map0),
	complex_to_andor(Complex,Map0,Map,Tree),
	%numbervars(Tree,1,_), writeln(Tree), fail ; true ),
	rb_visit(Map, MapList),
	%writeln(Tree),
	extract_vars(MapList, Vs),
	%List = [L1 = X2865 * X2445, L2 = X1747 * X2441],
	%MyMapList = [X2865-2865, X2445-2445, X1747-1747, X2441-2441],
	%List = [L1 = X2865 * X2445, L2 = X1747 * X2441, L3 = X1746 * X2441, L4 = X1745 * X2441, L5 = X3053 * X2447, L6 = X1744 * X2441, L7 = X2447 * X3052, L8 = X2446 * X3046, L9 = X2445 * X2864, L10 = X2444 * X2344, L11 = X2443 * X2269, L12 = X2442 * X1870, L13 = X2441 * X1743, L14 = X2440 * X1656, L15 = X2439 * X738, L16 = X2438 * X571, L17 = X2437 * X417, L18 = X2268 * X2443, L19 = X737 * X2439, L20 = X570 * X2438, L21 = X416 * X2437, L22 = X1742 * X2441, L23 = X2343 * X2444, L24 = X1654 * X2440, L25 = X1741 * X2441, L26 = X1740 * X2441, L27 = X2266 * X2443, L28 = X415 * X2437, L29 = X3051 * X2447, L30 = X1739 * X2441, L31 = X1738 * X2441, L32 = X2333 * X2444, L33 = X1645 * X2440, L34 = X3050 * X2447, L35 = X1737 * X2441, L36 = X2264 * X2443, L37 = X736 * X2439, L38 = X2330 * X2444, L39 = X2263 * X2443, L40 = X1642 * X2440, L41 = X735 * X2439, L42 = X411 * X2437, L43 = X2262 * X2443, L44 = X2329 * X2444, L45 = X2328 * X2444, L46 = X2863 * X2445, L47 = X3043 * X2446, L48 = X1735 * X2441, L49 = X2327 * X2444, L50 = X1640 * X2440, L51 = X1734 * X2441, L52 = X3049 * X2447, L53 = X1733 * X2441, L54 = X1732 * X2441, L55 = X3133 * L1, L56 = X2854 * L2, L57 = X2821 * L3, L58 = X2792 * L4, L59 = X2602 * L5, L60 = X2517 * L6, L61 = X2343 * L18, L62 = X2329 * L19, L63 = X2328 * L20, L64 = X2327 * L21, L65 = X2301 * L22, L66 = X2268 * L23, L67 = X2266 * L24, L68 = X2258 * L25, L69 = X2052 * L26, L70 = X1654 * L27, L71 = X1640 * L28, L72 = X1472 * L29, L73 = X1471 * L30, L74 = X1423 * L31, L75 = X1063 * L32, L76 = X1062 * L33, L77 = X1002 * L34, L78 = X1001 * L35, L79 = X975 * L36, L80 = X963 * L37, L81 = X813 * L38, L82 = X812 * L39, L83 = X810 * L40, L84 = X806 * L41, L85 = X805 * L42, L86 = X778 * L43, L87 = X737 * L44, L88 = X570 * L45, L89 = X531 * L46, L90 = X499 * L47, L91 = X421 * L48, L92 = X416 * L49, L93 = X415 * L50, L94 = X409 * L51, L95 = X366 * L52, L96 = X314 * L53, L97 = X45 * L54, L98 = L55 + L56 + L57 + L58 + L59 + L60 + L7 + L8 + L9 + L10 + L11 + L12 + L13 + L14 + L15 + L16 + L17 + L61 + L62 + L63 + L64 + L65 + L66 + L67 + L68 + L69 + L70 + L71 + L72 + L73 + L74 + L75 + L76 + L77 + L78 + L79 + L80 + L81 + L82 + L83 + L84 + L85 + L86 + L87 + L88 + L89 + L90 + L91 + L92 + L93 + L94 + L95 + L96 + L97],
	%MyMapList = [X1001-1001, X1002-1002, X1062-1062, X1063-1063, X1423-1423, X1471-1471, X1472-1472, X1640-1640, X1642-1642, X1645-1645, X1654-1654, X1732-1732, X1733-1733, X1734-1734, X1735-1735, X1737-1737, X1738-1738, X1739-1739, X1740-1740, X1741-1741, X1742-1742, X1744-1744, X1745-1745, X1746-1746, X1747-1747, X2052-2052, X2258-2258, X2262-2262, X2263-2263, X2264-2264, X2266-2266, X2268-2268, X2301-2301, X2327-2327, X2328-2328, X2329-2329, X2330-2330, X2333-2333, X2343-2343, X2437-2437, X2438-2438, X2439-2439, X2440-2440, X2441-2441, X2442-2442, X2443-2443, X2444-2444, X2445-2445, X2446-2446, X2447-2447, X2517-2517, X2602-2602, X2792-2792, X2821-2821, X2854-2854, X2863-2863, X2865-2865, X3043-3043, X3049-3049, X3050-3050, X3051-3051, X3053-3053, X3133-3133, X314-314, X366-366, X409-409, X411-411, X415-415, X416-416, X421-421, X45-45, X499-499, X531-531, X570-570, X735-735, X736-736, X737-737, X778-778, X805-805, X806-806, X810-810, X812-812, X813-813, X963-963, X975-975],
	%setof(X, in(List, X), Vs0),
	%writeln(vs0(Vs0)),
	%compact_list(Vs0, Vs),
	%writeln(vars(Vs)),
	%bdd_from_list(List, MyMapList, BDD).
	bdd_new(Tree, BDD), !. %writeln(BDD).

compact_list([], []).
compact_list([X-A| T], [X-A | Tail]) :-
		   delete(X-A, T, T1),
		   compact_list(T1, Tail).

in(T,V) :- var(T), !, fail.
in(T,V) :- integer(T), !, V = T-_.
in(T,V) :- T =.. [_|Ts], member(X,Ts), in(X,V).

tabled_trie_to_bdd(Trie, BDD, MapList) :-
    trie_to_list(Trie, Complex),
    tabled_list_to_trie(Complex, BDD, MapList).

tabled_list_to_trie(empty, BDD, []) :- !,
    bdd_new(0, BDD).
tabled_list_to_trie(Complex, BDD, MapList) :-
	rb_new(Map0),
	rb_new(Tab0),
	Complex = [list(Els)],
	tabled_complex_to_andor(Els,Map0,Map,Tab0,_Tab,Tree),
	rb_visit(Map, MapList),
	%extract_vars(MapList, Vs),
	bdd_new(Tree, BDD),
	bdd_tree(BDD, bdd(_, L, _)), length(L,Len), writeln(Len).

extract_vars([], []).
extract_vars((_-V).MapList, V.Vs) :-
	extract_vars(MapList, Vs).

%complex_to_andor(X, M0, M1, Y) :-  writeln(X), fail.
complex_to_andor(empty, Map, Map, 0).
% complex_to_andor([endlist], Map, Map, 0)  :- !.
complex_to_andor([list(Els)], Map0, MapF, Tree) :- !,
	complex_to_andor(Els, Map0, MapF, Tree).
complex_to_andor([V, [endlist]], Map0, MapF, Tree) :-
	complex_to_and(V, Map0, MapF, Tree), !.
complex_to_andor([El1,El2|Els], Map0, MapF, OR) :- !,
	complex_to_and(El1, Map0, MapI, T1),
	complex_to_andor([El2|Els], MapI, MapF, T2),
	simp_or(T1, T2, OR).
complex_to_andor([Els], Map0, MapF, V) :- 
	complex_to_and(Els, Map0, MapF, V).

simp_or(A1, _, 1) :-
	    A1 == 1, !.
simp_or(_, A2, 1) :-
	   A2 == 1, !.
simp_or(A1, A2, 0) :-
	    A1 == 0,
	    A2 == 0, !.
simp_or(X, Y, or(X, Y)).

simp_and(A1, _, 0) :-
	     A1 == 0, !.
simp_and(_, A2, 0) :-
	    A2 == 0, !.
simp_and(A1, A2, 1) :-
	     A1 == 1,
	     A2 == 1, !.
simp_and(X, Y, and(X, Y)).


%complex_to_and(X, M0, M1, Y) :-writeln(X), fail.
complex_to_and(endlist, Map, Map, 1) :- !.
complex_to_and(int(A1,[endlist]), Map0, MapF, V) :- !,
	check(Map0, A1, V, MapF).
complex_to_and(atom(true,[endlist]), Map0, MapF, 1) :- !.
complex_to_and(atom(A1,[endlist]), Map0, MapF, V) :- !,
	check(Map0, A1, V, MapF).
% complex_to_and(int(A1, L), Map0, MapF, V) :-
% 	memberchk(endlist, L),
% 	check(Map0, A1, V, MapF).
% complex_to_and(atom(A1, L), Map0, MapF, V) :-
% 	memberchk(endlist, L),
% 	check(Map0, A1, V, MapF).
complex_to_and(functor(not,1,[int(A1,[endlist])]), Map0, MapF, not(V)) :- !,
	check(Map0, A1, V, MapF).
complex_to_and(functor(not,1,[atom(A1,[endlist])]), Map0, MapF, not(V)) :- !,
	check(Map0, A1, V, MapF).
complex_to_and(int(A1,Els), Map0, MapF, AND) :-  !,
	check(Map0, A1, V, MapI),
	complex_to_andor(Els, MapI, MapF, T2),
	simp_and(V,T2,AND).
complex_to_and(atom(A1,Els), Map0, MapF, AND) :-  !,
	check(Map0, A1, V, MapI),
	complex_to_andor(Els, MapI, MapF, T2),
	simp_and(V,T2,AND).
complex_to_and(functor(not,1,[int(A1,Els)]), Map0, MapF, AND) :- !,
	check(Map0, A1, V, MapI),
	complex_to_andor(Els, MapI, MapF, T2),
	simp_and(not(V),T2,AND).
complex_to_and(functor(not,1,[atom(A1,Els)]), Map0, MapF, AND) :- !,
	check(Map0, A1, V, MapI),
	complex_to_andor(Els, MapI, MapF, T2),
	simp_and(not(V),T2,AND).
% HASH TABLE, it can be an OR or an AND.
complex_to_and(functor(not,1,[int(A1,Els)|More]), Map0, MapF, OR) :- 
	check(Map0, A1, V, MapI),
	(Els == [endlist]
	->
	  NOTV1 = not(V),
	  MapI = MapI2
	;
	  complex_to_andor(Els, MapI, MapI2, T2),
	  NOTV1 = and(not(V), T2)
	),
	complex_to_and(functor(not,1,More), MapI2, MapF, O2),
	simp_or(NOTV1, O2, OR).
complex_to_and(functor(not,1,[atom(A1,Els)|More]), Map0, MapF, OR) :- 
	check(Map0, A1, V, MapI),
	(Els == [endlist]
	->
	  NOTV1 = not(V),
	  MapI = MapI2
	;
	  complex_to_andor(Els, MapI, MapI2, T2),
	  NOTV1 = and(not(V), T2)
	),
	complex_to_and(functor(not,1,More), MapI2, MapF, O2),
	simp_or(NOTV1, O2, OR).

tabled_complex_to_andor(T, Map, Map, Tab, Tab, V) :- 
	rb_lookup(T, V, Tab), !,
	increment_ref_count(V).
tabled_complex_to_andor(IN, Map0, MapF, Tab0, TabF, OUT) :-
	IN = [El1,El2|Els], !,
	OUT = or(0,_,T1,T2),
	rb_insert(Tab0, IN, OUT, Tab1),
	tabled_complex_to_and(El1, Map0, MapI, Tab1, TabI, T1),
	tabled_complex_to_andor(El2.Els, MapI, MapF, TabI, TabF, T2).
tabled_complex_to_andor([Els], Map0, MapF, Tab0, TabF, V) :-
	tabled_complex_to_and(Els, Map0, MapF, Tab0, TabF, V).

tabled_complex_to_and(int(A1,[endlist]), Map0, MapF, Tab, Tab, V) :- !,
	check(Map0, A1, V, MapF).
tabled_complex_to_and(atom(A1,[endlist]), Map0, MapF, Tab, Tab, V) :- !,
	check(Map0, A1, V, MapF).
tabled_complex_to_and(functor(not,1,[int(A1,[endlist])]), Map0, MapF, Tab, Tab, not(V)) :- !,
	check(Map0, A1, V, MapF).
tabled_complex_to_and(functor(not,1,[atom(A1,[endlist])]), Map0, MapF, Tab, Tab, not(V)) :- !,
	check(Map0, A1, V, MapF).
tabled_complex_to_and(T, Map, Map, Tab, Tab, V) :- 
	rb_lookup(T, V, Tab), !,
	increment_ref_count(V).
tabled_complex_to_and(IN, Map0, MapF, Tab0, TabF, OUT) :- 
	IN = int(A1,Els), !,
	check(Map0, A1, V, MapI),
	rb_insert(Tab0, IN, OUT, TabI),
	OUT = and(0, _, V, T2),
	tabled_complex_to_andor(Els, MapI, MapF, TabI, TabF, T2).
tabled_complex_to_and(IN, Map0, MapF, Tab0, TabF, OUT) :- 
	IN = atom(A1,Els), !,
	check(Map0, A1, V, MapI),
	rb_insert(Tab0, IN, OUT, TabI),
	OUT = and(0, _, V, T2),
	tabled_complex_to_andor(Els, MapI, MapF, TabI, TabF, T2).
tabled_complex_to_and(IN, Map0, MapF, Tab0, TabF, OUT) :-
	IN = functor(not,1,[int(A1,Els)]), !,
	check(Map0, A1, V, MapI),
	rb_insert(Tab0, IN, OUT, TabI),
	OUT = and(0, _, not(V), T2),
	tabled_complex_to_andor(Els, MapI, MapF, TabI, TabF, T2).
tabled_complex_to_and(IN, Map0, MapF, Tab0, TabF, OUT) :-
	IN = functor(not,1,[atom(A1,Els)]), !,
	check(Map0, A1, V, MapI),
	rb_insert(Tab0, IN, OUT, TabI),
	OUT = and(0, _, not(V), T2),
	tabled_complex_to_andor(Els, MapI, MapF, TabI, TabF, T2).

check(M0, K, V, M) :- rb_lookup(K, V, M0), !, M = M0.
check(M0, K, V, M) :- rb_insert(M0, K, V, M).

increment_ref_count(V)  :-
	arg(1,V,I0),
	I is I0+1,
	setarg(1,V,I).


