z(I, EstPruningA, EvalPruningA, EstPruningO, EvalPruningO, Estimator, TestDir) :-
  yap_flag(verbose, silent),
  consult('/home/ubuntu/jcr-svn/trunk/SkILL_6.3/skill.yap'),
  atomic_concat('/home/ubuntu/jcr-svn/trunk/tests/athletes/5folds/train', I, Train),
  atomic_concat('/home/ubuntu/jcr-svn/trunk/tests/athletes/5folds/athlete_background', I, Met),
  atomic_concat('/home/ubuntu/jcr-svn/trunk/tests/athletes/5folds/prob_background', I, PBack),
  set_skill_option(prob_predicates, [athleteplayssport/2, teamplaysinleague/2, athleteplaysinleague/2, athletealsoknownas/2, coachesinleague/2, coachesteam/2, teamhomestadium/2, teamplayssport/2, athleteplayssportsteamposition/2, athletehomestadium/2, athletecoach/2]),
  set_skill_option(prob_background, [PBack]),
  consult(Train),
  % Record metrics
  Path = '/home/ubuntu/jcr-svn/trunk/tests/athletes/',
  atomic_concat([Path, TestDir, 'rmse_and', I], RMSEAnd),
  set_skill_option(record_rmse_and, RMSEAnd),
  atomic_concat([Path, TestDir, 'rmse_or', I], RMSEOr),
  set_skill_option(record_rmse_or, RMSEOr),
  atomic_concat([Path, TestDir, 'pacc_and', I], PACCAnd),
  set_skill_option(record_pacc_and, PACCAnd),
  atomic_concat([Path, TestDir, 'pacc_or', I], PACCOr),
  set_skill_option(record_pacc_or, PACCOr),
  atomic_concat([Path, TestDir, 'pvalues_and', I], PValuesAnd),
  set_skill_option(record_pvalues_and, PValuesAnd),
  atomic_concat([Path, TestDir, 'pvalues_or', I], PValuesOr),
  set_skill_option(record_pvalues_or, PValuesOr),
  % Fitness Pruning
  set_skill_option(and_primary_rank, pacc),
  set_skill_option(and_secondary_rank, pacc),
  set_skill_option(and_primary_size, 100000),
  set_skill_option(and_secondary_size, 100000),
  set_skill_option(or_primary_rank, pacc),
  set_skill_option(or_secondary_rank, pacc),
  set_skill_option(or_primary_size, 100000),
  set_skill_option(or_secondary_size, 100000),
  % 24 hour timeout
  set_skill_option(timeout, 86400),
  % Estimation Pruning
  set_skill_option(and_pruning_est, EstPruningA),
  set_skill_option(or_pruning_est, EstPruningO),
  %set_skill_option(estimator, Estimator),
  % Prediction Pruning
  set_skill_option(and_pruning_eval, EvalPruningA),
  set_skill_option(or_pruning_eval, EvalPruningO),
  % SkILL state
  atomic_concat(['aux', I], AuxFile),
  set_skill_option(varnumbers_file, AuxFile),
  atomic_concat(['/home/ubuntu/jcr-svn/trunk/tests/athletes/', TestDir, I], State),
  set_skill_option(state_file, State),
  run_skill(Met),
  halt.
z(I, _, _, _) :-
  writeln('FAILED'),
  halt.
