#!/bin/bash

# No pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,off,off,off,_,'ExhaustiveTests/aoff_ooff/state_exh_lt3_aoff_ooff_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Soft OR prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,off,off,soft,_,'ExhaustiveTests/aoff_osoft/state_exh_lt3_aoff_osoft_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Hard OR prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,off,off,hard,_,'ExhaustiveTests/aoff_ohard/state_exh_lt3_aoff_ohard_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5 

# Soft AND prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,soft,off,off,_,'ExhaustiveTests/asoft_ooff/state_exh_lt3_asoft_ooff_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Soft AND and OR prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,soft,off,soft,_,'ExhaustiveTests/asoft_osoft/state_exh_lt3_asoft_osoft_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Soft AND and hard OR prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,soft,off,hard,_,'ExhaustiveTests/asoft_ohard/state_exh_lt3_asoft_ohard_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Hard AND prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,hard,off,off,_,'ExhaustiveTests/ahard_ooff/state_exh_lt3_ahard_ooff_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Hard AND and soft OR prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,hard,off,soft,_,'ExhaustiveTests/ahard_osoft/state_exh_lt3_ahard_osoft_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5

# Hard AND and OR prediction pruning
for i in {1..5}
do
  $HOME/jcr-svn/trunk/yap-6.3-master/compile_dir/yap -s 10000000 -l run_metabolism.yap -z "z($i,off,hard,off,hard,_,'ExhaustiveTests/ahard_ohard/state_exh_lt3_ahard_ohard_')" &
  sleep 5
done
wait                                                                        
# killall -9 yap                                                                  
sleep 5 

