:- consult('population.yap').

smart_strategy_or([], []) :- !.
smart_strategy_or(_, []) :-
  check_skill_option(length_of_theory, 1).
smart_strategy_or(Population, BestTheory) :-
  n_best_rules_from_list(Population, 1, [BestRule]),
  smart_strategy_or_aux(Population, 1, BestRule, BestTheory).

smart_strategy_or_aux(Population, LengthOfTheory, PreviousBest, Best) :-
  check_skill_option(length_of_theory, MaxLength),
  LengthOfTheory < MaxLength,
  theory_loop(PreviousBest, Population, NewBestHyp),
  length(NewBestHyp, Len),
  (
  Len > 0 ->
  NewLengthOfTheory is LengthOfTheory + 1,
  smart_strategy_or_aux(Population, NewLengthOfTheory, NewBestHyp, Best)
  ;
  Best = PreviousBest
  ).
smart_strategy_or_aux(_, _, BestHyp, BestHyp).

theory_loop(BestTheory, Population, NewBestHyp) :-
  find_complementary_values(BestTheory, ComplementaryValues),
  check_skill_option(smart_strategy, Strategy),
  theory_loop_aux_ss(Strategy, BestTheory, ComplementaryValues, Population, NewBestHyp).
    
theory_loop_aux_ss(distance, BestTheory, ComplementaryValues, Rules, NewBestHyp) :-
  create_list_of_distances(ComplementaryValues, Rules, [], OrderedListOfDistances),
  theory_loop_aux_distance(BestTheory, OrderedListOfDistances, NewBestHyp).
  
theory_loop_aux_distance(_, [], []) :-!.
theory_loop_aux_distance(BestTheory, [dist(Rule, _, _, _) | _Rules], NewBestHyp) :-
  Rule = hyp(Hyp1, _, _, _, _, _),
  BestTheory = hyp(Hyp2, _, _, _, _, _),
  append(Hyp1, Hyp2, ThisTheory),
  once(evaluation(ThisTheory, ThisEval)),
  rule_value(ThisEval, ThisResult),
  rule_value(BestTheory, BestResultSoFar),
  ThisResult < BestResultSoFar, !,
  writeln('BETTER'),
  NewBestHyp = ThisEval.
theory_loop_aux_distance(BestTheory, [_Rule | Rules], NewBestHyp) :-
  theory_loop_aux_distance(BestTheory, Rules, NewBestHyp).
  
create_list_of_distances(_, [], OrderedListOfDistances, OrderedListOfDistances) :-!.
create_list_of_distances(ComplementaryValues, [Rule | Rules], ListOfDistances, OrderedListOfDistances) :-
  Rule = hyp(_, RuleValues, _, _, _, _),
  find_distances_one_rule(ComplementaryValues, RuleValues, 0, 0, 0, PosDist, NegDist, HellDist),
  insert_element_in_list(dist(Rule, PosDist, NegDist, HellDist), ListOfDistances, [], NewListOfDistances),
  create_list_of_distances(ComplementaryValues, Rules, NewListOfDistances, OrderedListOfDistances).
  
find_distances_one_rule([], [], PosDist, NegDist, HD, PosDist, NegDist, HellDist) :-
  HellDist is sqrt(HD)/sqrt(2), !.
find_distances_one_rule([CValue | CValues], [RValue | RValues], PD, ND, HD, PosDist, NegDist, HellDist) :-
  (
    RValue > CValue ->
    NewPD is PD + RValue - CValue,
    NewND = ND
  ;
    NewPD = PD,
    NewND is ND + CValue - RValue
  ),
  NewHD is HD + (sqrt(CValue) - sqrt(RValue))^2,
  find_distances_one_rule(CValues, RValues, NewPD, NewND, NewHD, PosDist, NegDist, HellDist).
  
insert_element_in_list(dist(Rule, PosDist, NegDist, HellDist), [dist(R, PD, ND, DH) | Distances], OrderedDistances, OrderedListOfDistances) :-
  PosDist > PD,
  append(OrderedDistances, dist(R, PD, ND, DH), NewOrderedDistances),
  insert_element_in_list(dist(Rule, PosDist, NegDist, HellDist), Distances, NewOrderedDistances, OrderedListOfDistances).
insert_element_in_list(Rule, Distances, OrderedDistances, OrderedListOfDistances) :-
  append(OrderedDistances, [Rule], NewOrderedDistances),
  append(NewOrderedDistances, Distances, OrderedListOfDistances).
  
theory_loop_aux(threshold, _, _, [], []) :-!.
theory_loop_aux(threshold, BestTheory, ComplementaryValues, [Rule | _Rules], NewBestHyp) :-
  is_rule_complementary(Rule, ComplementaryValues),
  Rule = hyp(Hyp1, _, _, _, _, _),
  BestTheory = hyp(Hyp2, _, _, _, _, _),
  append(Hyp1, Hyp2, ThisTheory),
  once(evaluation(ThisTheory, ThisEval)),
  rule_value(ThisEval, ThisResult),
  rule_value(BestTheory, BestResultSoFar),
  ThisResult < BestResultSoFar, !,
  NewBestHyp = ThisEval.
theory_loop_aux(threshold, BestTheory, ComplementaryValues, [_ | Rules], NewBestHyp) :-
  theory_loop_aux(threshold, BestTheory, ComplementaryValues, Rules, NewBestHyp).

find_complementary_values(BestTheory, ComplementaryValues) :-
  BestTheory = hyp(_, PValues, _, _, _, _),
  exp_values(ExValues),
  check_skill_option(estimator, Estimator),
  estimate_all_points(PValues, ExValues, Estimator, ComplementaryValues).
    
estimate_all_points([], [], _, []) :-!.
estimate_all_points([PValue | PValues], [ExValue | ExValues], Estimator, [CValue | CValues]) :-
  %estimate_pseudo_point(Estimator, or, PValue, ExValue, CValue),
  estimate_pseudo_point(center, or, PValue, ExValue, CValue),
  estimate_all_points(PValues, ExValues, Estimator, CValues).
    
is_rule_complementary(Rule, ComplementaryValues) :-
  check_skill_option(complementary_threshold, Threshold),
  Rule = hyp(_, PValues, _, _, _, _),
  check_all_points(PValues, ComplementaryValues, Threshold), !.
    
check_all_points([], [], _) :- !.
check_all_points([_PValue | PValues], [CValue | CValues], Threshold) :-
  var(CValue), !,
  check_all_points(PValues, CValues, Threshold).
check_all_points([PValue | PValues], [CValue | CValues], Threshold) :-
  Max is CValue + Threshold,
  Min is CValue - Threshold,
  PValue >= Min,
  PValue =< Max, !,
  check_all_points(PValues, CValues, Threshold).
    
%estimate_pseudo_point(center, and, Point1, Point2, Result) :-
  %Min is 0,
  %Max is min(Point1, Point2),
  %Result is (Min + Max) / 2.
  
estimate_pseudo_point(center, or, PValue, ExValue, Res) :-
  PValue =< ExValue, !,
  Value is 2 * ExValue - 2 * PValue,
  Aux is max(Value, 0),
  Res is min(Aux, 1).
estimate_pseudo_point(center, or, PValue, ExValue, Res) :-
  Value is (2 * ExValue - PValue) / 2,
  Aux is max(Value, 0),
  Res is min(Aux, 1).

%estimate_pseudo_point(independence, and, Point1, Point2, Result) :-
  %Result is Point1 * Point2.

estimate_pseudo_point(independence, or, PValue, ExValue, Res) :-
  Value is (ExValue - PValue) / (1 - PValue),
  Aux is max(Value, 0),
  Res is min(Aux, 1).
  
%estimate_pseudo_point(max, and, Point1, Point2, Result) :-
  %Result is min(Point1, Point2).

estimate_pseudo_point(max, or, PValue, ExValue, Res) :-
  PValue =< ExValue, !,
  Value is ExValue - PValue,
  Aux is max(Value, 0),
  Res is min(Aux, 1).
  
/*estimate_pseudo_point(min, and, Point1, Point2, Result) :-
  Dif is Point1 + Point2 - 1,
  Result is max(Dif, 0). */ 

estimate_pseudo_point(min, or, PValue, ExValue, Res) :-
  PValue >= ExValue, !,
  Value is ExValue,
  Aux is max(Value, 0),
  Res is min(Aux, 1).  
    
%estimate_pseudo_point(exclusion, and, Point1, Point2, Result) :-
  %estimate_pseudo_point(min, and, Point1, Point2, Result).
  
estimate_pseudo_point(exclusion, or, PValue, ExValue, Res) :-
  estimate_pseudo_point(max, or, PValue, ExValue, Res).
  
estimate_pseudo_point(_, _, _, _, _).