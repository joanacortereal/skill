:- consult('evaluation.yap').
:- consult('hypotheses1.yap').
:- consult('utils.yap').
:- consult('population.yap').
:- consult('smart_strategy.yap').
:- consult('pruning.yap').
:- consult('skill.yap').

%multifile this?
user:term_expansion(prob_example(Ex, Weight, Prob), [example(Ex, Weight), prob_example(Ex, Weight, Prob)]).

% run_skill_seq(+File)
% runs skill solving the problem in File
% uses toplog in GILPS to find hypotheses of one clause
run_skill_seq(File) :-
  run_skill_seq(File, _).
run_skill_seq(File, BestHyp) :-
  statistics(walltime, [TIwall, _]),
  assert(initial_time(TIwall)),
  statistics(cputime, [TIcpu, _]),
  datime(datime(_Years, _Months, _Days, HoursI, MinutesI, SecondsI)),
  prepare_stochastic_component,
  hypotheses_size_one(File, ListOfHyps1),
  save_state(population, 1, ListOfHyps1),
  %%%%%% fix this for nice tune set %%%%%%%%%%%%%%
  check_skill_option(tune_set, Tune),
  (
     Tune == no -> !
  ;
    abolish(prob_example, 3),
    consult(Tune),
    update_queries
  ),
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  assert_example_data,
  construct_population(ListOfHyps1, Population, BestHyp1),
  save_state(evaluated, 1, Population),
  save_state(best, 1, BestHyp1),
  %%%%%%% FIX THIS %%%%%%
  %consult('skill1.yap'),
  %evaluated(1, Population),
  %best(1, BestHyp1),
  %%%%%%%%%%%%%%%%%%%%%%%
  (
  check_skill_option(search, smart) -> !,
  smart_strategy_or(Population, BestHypN)
  ;
  construct_theory(Population, BestHypN)
  ),
  append(BestHyp1, BestHypN, BestHyps),
  check_skill_option(evaluation, Evaluation),
  n_best_rules_from_list(Evaluation, BestHyps, 1, [BestHyp]),
  BestHyp = hyp(Hyp, _PValues, _Diff, _PAcc, _RMSE, _TAcc),
  (
    check_skill_option(test_set, no) -> !,
    EvalBestHyp = BestHyp
  ;
    check_skill_option(test_set, Test),
    abolish(prob_example, 3),
    consult(Test),
    update_queries,
    evaluation(Hyp, EvalBestHyp)
  ),
  EvalBestHyp = hyp(FHyp, FPValues, FDiff, FPAcc, FRMSE, FTAcc),
  writeln('------- RESULT -------'),
  (
    nonvar(FHyp) ->
    numbervars(FHyp, 0, _), 
    writeln(hypotheses(FHyp)),
    PAccAux is (1-FPAcc)*100,
    writeln(pacc(PAccAux)),
    writeln(rmse(FRMSE)),
    writeln(tacc(FTAcc))
  ;
    writeln('No theory found'),
    FPValues = -1,
    FDiff = -1,
    FPAcc = -1,
    FRMSE = -1,
    FTAcc = -1
  ),
  statistics(walltime, [TFwall, _]),
  statistics(cputime, [TFcpu, _]),
  Timewall is TFwall - TIwall,
  Timecpu is TFcpu - TIcpu,
  datime(datime(_Years, _Months, _Days, HoursF, MinutesF, SecondsF)),
  ExecutionTime is (HoursF - HoursI) * 60 *60 + (MinutesF - MinutesI) * 60 + (SecondsF - SecondsI),
  save_state(cputime, final, Timecpu),
  save_state(walltime, final, Timewall),
  save_state(execution_time, final, ExecutionTime),
  get_value(eval_counter, EvaluationCounter),
  get_value(est_counter_and, EstCounterAnd),
  get_value(est_counter_or, EstCounterOr),
  get_value(eval_counter_and, EvalCounterAnd),
  get_value(eval_counter_or, EvalCounterOr),
  get_value(timeout_counter, Timeout),
  save_state(eval_counter, final, EvaluationCounter),
  save_state(est_counter_and, final, EstCounterAnd),
  save_state(est_counter_or, final, EstCounterOr),
  save_state(eval_counter_and, final, EvalCounterAnd),
  save_state(eval_counter_or, final, EvalCounterOr),
  save_state(timeout_counter, final, Timeout),
  save_state(best, final, hyp(FHyp, FPValues, FDiff, FPAcc, FRMSE, FTAcc)),
  (
    check_skill_option(remote_evaluation, yes) ->
    check_skill_option(socket, Socket),
    jcr_write_to_socket(Socket, die)
  ;
    true
  ).
  

%%%%%%%%%%%%%%%%%%%%%%%% LOOP OVER LITERAL SPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

construct_population(AllHyps1Aux, Population, BestHyp1) :-
  %select_basic_rules(AllHyps1, OtherHypsAux, Hyps1LitAux),
  
  correct_format(AllHyps1Aux, AllHyps1),
  find_all_rules_size(AllHyps1, 1, OtherHyps, Hyps1Lit),
  
  %correct_format(Hyps1LitAux, Hyps1Lit),
  %correct_format(OtherHypsAux, OtherHyps),
  evaluate_list(Hyps1Lit, EvalHyps1LitAux),
  %why is this flatten necessary?!
  flatten(EvalHyps1LitAux, Population1),
    
  get_value(eval_counter, NoRules1),
  save_state(no_rules_1, final, NoRules1),
  writeln('finished lit1 evaluations'),
  prediction_pruning(and, Population1, PPopulation1),
  fitness_pruning(and, secondary, PPopulation1, FPPopulation1),
  construct_population_loop(OtherHyps, FPPopulation1, PPopulation1, 2, RestOfPopulation),
  record_metrics_and(RestOfPopulation),
  append(Population1, RestOfPopulation, Population),
  n_best_rules_from_list(Population, 1, BestHyp1),
  writeln('finished rule evaluation'),
  get_value(eval_counter, NoRules),
  save_state(no_rules, final, NoRules).
  
record_metrics_and([]) :-!.
record_metrics_and([hyp(_, ProbValues, _, PAcc, RMSE, _) | T]) :-
  record_rmse_and(RMSE),
  record_pacc_and(PAcc),
  record_pvalues_and(ProbValues),
  record_metrics_and(T).
  
record_rmse_and(RMSE) :-
  check_skill_option(record_rmse_and, File),
  (
    File == no ->
    true
  ;
    open(File, append, S),
    write(S, RMSE), write(S, '\n'),
    close(S)
  ).
  
record_pacc_and(PAcc) :-
  check_skill_option(record_pacc_and, File),
  (
    File == no ->
    true
  ;
    open(File, append, S),
    write(S, PAcc), write(S, '\n'),
    close(S)
  ).
  
record_pvalues_and(ProbValues) :-
  check_skill_option(record_pvalues_and, File),
  (
    File == no ->
    true
  ;
    open(File, append, S),
    write_to_csv(S, ProbValues),
    close(S)
  ).
  
write_to_csv(S, [H]) :-
  write(S, H),
  write(S, '\n').
write_to_csv(S, [H | T]) :-
  write(S, H),
  write(S, ','),
  write_to_csv(S, T).
  
print1perline([]) :-!.
print1perline([H | T]) :-
  writeln(H),
  print1perline(T).
  
construct_population_loop([], _, _, _, []) :- !.
construct_population_loop(ThisPopulation, PrunedLit1Population, PreviousPopulation, NoLits, ThisEvalPopulation) :-
  fitness_pruning(and, primary, PreviousPopulation, PrunedPreviousPopulation),
  find_all_rules_size(ThisPopulation, NoLits, Rest, PossibleCombinations),
  iterative_evaluation_conjunction(PrunedPreviousPopulation, PrunedLit1Population, PossibleCombinations, PopulationN),
  prediction_pruning(and, PopulationN, PPopulationN),
  NewNoLits is NoLits + 1,
  construct_population_loop(Rest, PrunedLit1Population, PPopulationN, NewNoLits, PopulationNplus1),
  append(PopulationN, PopulationNplus1, ThisEvalPopulation).
  
% if no rules of size N-1 were evaluated, no rules of size N will be
iterative_evaluation_conjunction(_, _, [], []) :- !.
iterative_evaluation_conjunction(PreviousPopulation, Lit1Population, [CombsH | CombsT], EvalCombs) :- !,
  iterative_evaluation_conjunction1(PreviousPopulation, Lit1Population, CombsH, EvalComb1),
  iterative_evaluation_conjunction(PreviousPopulation, Lit1Population, CombsT, OtherEvalCombs),
  append(EvalComb1, OtherEvalCombs, EvalCombs).
  
iterative_evaluation_conjunction1(PreviousPopulation, Lit1Population, ConjH, [EvalConjH]) :-
  once(
    (split_rule(ConjH, Lit1Population, PreviousPopulation, RuleIn1, RuleInPrevious),
    estimation_pruning(and, RuleIn1, RuleInPrevious),
    ConjH = hyp(Hyp, _, _, _, _, _),
    evaluation(Hyp, EvalConjH))
    ).
iterative_evaluation_conjunction1(_, _, _, []).
  
split_rule(hyp([Rule], _, _, _, _, _), Rules1, RulesP, Rule1, RuleP) :-
  find_in_saved_rules(Rule, Rules1, Rest, Rule1),
  find_in_saved_rules(Rest, RulesP, _, RuleP).
  
find_in_saved_rules(RuleAux, [Rules1H | Rules1T], Rest, Rules1H) :-
  Rules1H = hyp([Rules1HClauseAux], _, _, _, _, _),
  my_varnumbers(Rules1HClauseAux, Rules1HClause),
  my_varnumbers(RuleAux, Rule),
  decompose_clause(Rules1HClause, Head, Rules1HBody, _Args, _Name, _Arity),
  list2Body(Rules1HList, Rules1HBody),
  decompose_clause(Rule, _Head, RuleBody, _Args, _Name, _Arity),
  list2Body(RuleList, RuleBody),
  sublist(Rules1HList, RuleList),
  list_difference(RuleList, Rules1HList, RestList),
  list2Body(RestList, RestBody),
  Rest = (Head:-RestBody),
  numbervars(Rules1H, 0, _),
  numbervars(Rest, 0, _).
find_in_saved_rules(Rule, [_ | Rules1T], Rest, Rule1) :-
  find_in_saved_rules(Rule, Rules1T, Rest, Rule1).

  
evaluate_list([], []) :-!.
evaluate_list([hyp(List, _, _, _, _, _) | T], [EvalH | EvalT]) :-
  once(evaluation(List, EvalH)),
  evaluate_list(T, EvalT).
evaluate_list([_ | T], [Eval]) :-
  evaluate_list(T, Eval).

select_basic_rules(AllRules, OtherRules, BasicRules) :-
  select_rules_sizeN(AllRules, 1, RulesN, Rules1),
  select_basic_rulesN(Rules1, Rules1, RulesN, 2, BasicRulesN),
  append(Rules1, BasicRulesN, BasicRules),
  list_difference(AllRules, BasicRules, OtherRules).
  
select_rules_sizeN([], _, [], []) :-!.
select_rules_sizeN([Rule | Rules], Size, RulesN, [Rule | Rules1]) :-
  decompose_clause(Rule, _Head, Body, _Args, _Name, _Arity),
  list2Body(BodyList, Body),
  length(BodyList, Size), !,
  select_rules_sizeN(Rules, Size, RulesN, Rules1).
select_rules_sizeN([Rule | Rules], Size, [Rule | RulesN], Rules1) :-
  select_rules_sizeN(Rules, Size, RulesN, Rules1).
  
select_basic_rulesN(_, _, [], _, []) :- !.
select_basic_rulesN(Rules1, PreviousRules, AllRules, Size, BasicRules) :-
  select_rules_sizeN(AllRules, Size, RulesRest, RulesSize),
  select_basic_rulesN_loop(RulesSize, Rules1, PreviousRules, BasicRulesN),
  NewSize is Size + 1,
  select_basic_rulesN(Rules1, BasicRulesN, RulesRest, NewSize, BasicRulesN1),
  append(BasicRulesN, BasicRulesN1, BasicRules).
  
select_basic_rulesN_loop([], _, _, []) :- !.
select_basic_rulesN_loop([Rule | Rules], Rules1, PreviousRules, BasicRulesN) :-
  rule_can_be_divided(Rule, Rules1, PreviousRules, _, _),
  select_basic_rulesN_loop(Rules, Rules1, PreviousRules, BasicRulesN).
select_basic_rulesN_loop([Rule | Rules], Rules1, PreviousRules, [Rule | BasicRulesN]) :-
  select_basic_rulesN_loop(Rules, Rules1, PreviousRules, BasicRulesN).
  
rule_can_be_divided(Rule, Rules1, PreviousRules, Rule1, RuleN) :-
  once(decompose_clause(Rule, Head, Body, _Args, _Name, _Arity)),
  once(list2Body(BodyList, Body)),
  sublist(BodyList1, BodyList),
  length(BodyList1, 1),
  list2Body(BodyList1, Body1),
  Rule1 = (Head:-Body1),
  BodyList1 = [Member],
  memberchk(Rule1, Rules1),
  delete(BodyList, Member, BodyListN),
  list2Body(BodyListN, BodyN),
  RuleN = (Head:-BodyN),
  memberchk(RuleN, PreviousRules).
  
  
% find_all_rules_size(+ListOfRules, -ListOfRulesWithLength>1, -ListOfRulesWithLength1)
%predicate to select all rules of only one literal or all blocks that contain one literal linked to the head and other literals that are unrelated to the head
%find_all_rules_size([], [], []) :-!.
% rules whose body has only 1 literal are kept
%find_all_rules_size([RulesH | RulesT], OtherRules, [RulesH | Rules1]) :-
  %RulesH = hyp([Clause], _, _, _, _, _),
  %decompose_clause(Clause, _Head, Body, _Args, _Name, _Arity),
  %list2Body(BodyList, Body),
  %length(BodyList, 1), !,
  %find_all_rules_size(RulesT, OtherRules, Rules1).
% rules that have several literals, but only one of them is connected to the head variable
find_all_rules_size([], _, [], []) :- !.
find_all_rules_size([RulesH | RulesT], Size, OtherRules, [RulesH | Rules1]) :-
  RulesH = hyp([Clause], _, _, _, _, _),
  decompose_clause(Clause, Head, BodyLits, _Args, _Name, _Arity),
  list2Body(Body, BodyLits),
  g_variables(Head, VarsHead),
  find_all_rules_size_aux(Body, 0, TotalNoLits, VarsHead),
  TotalNoLits == Size, !,
  find_all_rules_size(RulesT, Size, OtherRules, Rules1).
% other rules
find_all_rules_size([RulesH | RulesT], Size, [RulesH | OtherRules], Rules1) :-
  find_all_rules_size(RulesT, Size, OtherRules, Rules1).
  
find_all_rules_size_aux([], TotalNoLits, TotalNoLits, _) :- !. 
find_all_rules_size_aux([LitH | LitT], CurrNoLits, TotalNoLits, VarsHead) :-
  g_variables(LitH, Vars),
  (
    intersection(Vars, VarsHead, [_ | _]) -> !,
    NewNoLits is CurrNoLits + 1
  ;
    NewNoLits = CurrNoLits
  ),
  find_all_rules_size_aux(LitT, NewNoLits, TotalNoLits, VarsHead).
  
g_variables(Clause, OVars) :-
  g_variables(Clause, Vars, []),
  sort(Vars, OVars).
g_variables('$VAR'(I)) --> !, ['$VAR'(I)].
g_variables(T) --> {atomic(T)}.
g_variables(T) --> {T =.. [_ | L]},
  gs_variables(L).
  
gs_variables([]) --> [].
gs_variables([A | As]) -->
  g_variables(A),
  gs_variables(As).
  
sort_rules1([], _, []) :-!.
sort_rules1(AllRules, NoLits, SortedRules) :-
  select_members_no_lit(NoLits, AllRules, WrongNoLits, RightNoLits),
  NewNoLits is NoLits + 1,
  sort_rules1(WrongNoLits, NewNoLits, OtherSortedRules),
  append(OtherSortedRules, RightNoLits, SortedRules).
  
% find a better way to do this
find_all_lits1(AllHyps1, OtherHyps, Hyps1Lit) :-
  merge_all_hyps(AllHyps1, MergedHypsDup),
  remove_duplicates(MergedHypsDup, MergedHyps),
  AllHyps1 = [hyp([Clause], _, _, _, _, _) | _],
  decompose_clause(Clause, Head, _, _, _, _),
  numbervars(Head, 0, _),
  add_head(Head, MergedHyps, Hyps1Lit),
  %remove from population
  select_members_no_lit(1, AllHyps1, OtherHyps, _Hyps1).
  
merge_all_hyps([], []) :- !.
merge_all_hyps([hyp([Clause], _, _, _, _, _) | AllHyps1T], Output) :-
  %head always comes first so the variables there will always be A, B, ...
  numbervars(Clause, 0, _),
  literals2clause(Literals, Clause),
  Literals = [_Head | Body],
  merge_all_hyps(AllHyps1T, Rest),
  append(Body, Rest, Output).
  
add_head(_, [], []) :- !.
add_head(Head, [Body | Lit1T], [hyp([Clause], _, _, _, _, _) | T]) :-
  ClauseAux = (Head :- Body),
  my_varnumbers(ClauseAux, Clause),
  add_head(Head, Lit1T, T).
  
  
select_members_no_lit(_, [], [], []) :-!.
select_members_no_lit(NoLits, [hyp([List], _, _, _, _, _) | T], Rest, [hyp([List], _, _, _, _, _) | PossibleT]) :-
  literals2clause(ListAux, List),
  NoLitsAux is NoLits + 1,
  length(ListAux, NoLitsAux), !,
  select_members_no_lit(NoLits, T, Rest, PossibleT).
select_members_no_lit(NoLits, [H | T], [H | RestT], PossibleCombinations) :-
  select_members_no_lit(NoLits, T, RestT, PossibleCombinations).
  
  
find_rule_in_rules1([Rule1 | _Rules1], ThisRule, Rule1, [HeadOfRest | BodyOfRest]) :-
  Rule1 = hyp([Rule1Clause], _, _, _, _, _),
  ThisRule = hyp([ThisRuleClause], _, _, _, _, _),
  literals2clause(Rule1Lits, Rule1Clause),
  literals2clause(ThisRuleLits, ThisRuleClause),
  my_varnumbers(Rule1Lits, Rule1LitsAux),
  my_varnumbers(ThisRuleLits, ThisRuleLitsAux),
  %numbervars(Rule1LitsAux, 0, _),
  %numbervars(ThisRuleLitsAux, 0, _),
  once(sublist(Rule1LitsAux, ThisRuleLitsAux)), !,
  ThisRuleLitsAux = [HeadOfRest | _],
  list_difference(ThisRuleLitsAux, Rule1LitsAux, BodyOfRest).
find_rule_in_rules1([_Rule1 | Rules1], ThisRule, Rule, RestOfRule) :-
  find_rule_in_rules1(Rules1, ThisRule, Rule, RestOfRule).
  
list_difference(List, [], List) :-!.
list_difference(List, [Elem | Elems], Dif) :-
  memberchk(Elem, List), !,
  delete1(List, Elem, [], Rest),
  list_difference(Rest, Elems, Dif).
  
delete1([], _, Rest, Rest) :-!.
delete1([H | T], Elem, Beginning, Rest) :-
  H == Elem, !,
  append(Beginning, T, Rest).
delete1([H | T], Elem, Beginning, Rest) :-
  delete1(T, Elem, [H | Beginning], Rest).
  
% fix quick fix
%find_rule_in_previous_population([], _, _) :-
  %assert(evaluation_fix), !.
find_rule_in_previous_population([PrevRule | _PrevRules], ThisRuleLits, PrevRule) :-
  PrevRule = hyp([PrevRuleClause], _, _, _, _, _),
  literals2clause(PrevRuleLits, PrevRuleClause),
  my_varnumbers(PrevRuleLits, PrevRuleLitsAux),
  my_varnumbers(ThisRuleLits, ThisRuleLitsAux),
  %numbervars(PrevRuleLitsAux, 0, _),
  %numbervars(ThisRuleLitsAux, 0, _),
  sublist(ThisRuleLitsAux, PrevRuleLitsAux), !.
find_rule_in_previous_population([_PrevRule | PrevRules], ThisRule, Rule) :-
  find_rule_in_previous_population(PrevRules, ThisRule, Rule).
  
% fails if doesn't find anything - is that possible?
%find_hyp_in_previous([hyp([SmallList], PV, D, PAcc, RMSE, TAcc) | _TPrevious], hyp([List], _, _, _, _, _), hyp([SmallList], PV, D, PAcc, RMSE, TAcc)) :-
  %literals2clause(SmallListLit, SmallList),
  %literals2clause(ListLit, List),
  %varnumbers(SmallListLit, SmallListLitAux),
  %varnumbers(ListLit, ListLitAux),
  %sublist(SmallListLitAux, ListLitAux), !.
%find_hyp_in_previous([_HPrevious | TPrevious], Conj, SmallHyp) :-
  %find_hyp_in_previous(TPrevious, Conj, SmallHyp).
  
%find_lit_to_add(Lit1Population, hyp([List], _PV, _D, _PAcc, _RMSE, _TAcc), hyp([SmallList], _, _, _, _, _), EvalLit1) :-
  %literals2clause(ListLit, List),
  %literals2clause(SmallListLit, SmallList),
  %varnumbers(SmallListLit, SmallListLitAux),
  %varnumbers(ListLit, ListLitAux),
  %%varnumbers(Lit1Population, Lit1PopulationAux),
  %delete_elements(ListLitAux, SmallListLitAux, Lit1),
  %select_from_lit1(Lit1Population, Lit1, EvalLit1).
  
delete_elements([ListH | ListT], SmallList, Rest) :-
  memberchk(ListH, SmallList), !,
  delete_elements(ListT, SmallList, Rest).
delete_elements([ListH | _ListT], _SmallList, ListH).

select_from_lit1([hyp([List], PValues, Diff, PAcc, RMSE, TAcc) | _T], Lit1, hyp([List], PValues, Diff, PAcc, RMSE, TAcc)) :-
  decompose_clause(List, _Head, Body, _Args, _Name, _Arity),
  Body = Lit1, !.
select_from_lit1([_H | T], Lit1, EvalLit1) :-
  select_from_lit1(T, Lit1, EvalLit1).
  
find_sum_of_metrics([], 0, 0) :- !.
find_sum_of_metrics([hyp(_, _, _, PAcc, RMSE, _) | T], SumPAcc, SumRMSE) :-
  find_sum_of_metrics(T, OtherSumPAcc, OtherSumRMSE),
  SumPAcc is OtherSumPAcc + PAcc,
  SumRMSE is OtherSumRMSE + RMSE.
  
%%%%%%%%%%%%%%%%%%%%% THEORY LOOP OVER HYP SPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% construct_theory_par (+[Hyps1], -BestEvaluatedHyp)
% loops over all theory sizes to fing the best hypothesis
construct_theory([], []) :- !.
construct_theory(_, []) :-
  check_skill_option(length_of_theory, 1).
construct_theory(Population, BestHyp) :-
  % user options
  check_skill_option(length_of_theory, LengthOfTheory),
  check_skill_option(or_primary_rank, PrimaryRank),
  check_skill_option(or_secondary_rank, SecondaryRank),
  check_skill_option(or_primary_size, PrimarySize),
  check_skill_option(or_secondary_size, SecondarySize),
  prediction_pruning(or, Population, PPopulation),
  theory_loop(PrimaryRank, SecondaryRank, PPopulation, PPopulation, PrimarySize, SecondarySize, 2, LengthOfTheory, BestHypsList),
  n_best_rules_from_list(BestHypsList, 1, BestHyp).
 
  
%loops until size of theory is reached
%include threshold later!
theory_loop( _, _, _, _, _, _, _, 1, []) :- !.
theory_loop(_PrimaryRank, _SecondaryRank, _PrimaryPopulation, _SecondaryPopulation, _PrimarySize, _SecondarySize, NoClauses, LengthOfTheory, []) :-
  NoClauses > LengthOfTheory, !.
theory_loop(PrimaryRank, SecondaryRank, PrimaryPopulation, SecondaryPopulation, PrimarySize, SecondarySize, NoClauses, LengthOfTheory, BestTheories) :-
  select_population(PrimaryRank, SecondaryRank, PrimaryPopulation, SecondaryPopulation, PrimarySize, SecondarySize, Primary, Secondary),
  theory_loop_N_aux(Primary, Secondary, NoClauses, NewPrimaryPopulation, BestLocalList),
  prediction_pruning(or, NewPrimaryPopulation, PNewPrimaryPopulation),
  n_best_rules_from_list(BestLocalList, 1, BestLocal),
  NewNoClauses is NoClauses + 1,
  theory_loop(PrimaryRank, SecondaryRank, PNewPrimaryPopulation, Secondary, PrimarySize, SecondarySize, NewNoClauses, LengthOfTheory, BestTheoriesTail),
  append(BestLocal, BestTheoriesTail, BestTheories).
theory_loop(_, _, _, _, _, _, _, _, []).
  
%iterates over primary population
theory_loop_N_aux([], _, _, [], []) :-!.
theory_loop_N_aux([PH | PT], Secondary, NoClauses, NewPopulation, Best) :-
  iterative_evaluation(PH, Secondary, EvalRules),
  save_state(evaluated, NoClauses, EvalRules),
  record_metrics_or(EvalRules),
  n_best_rules_from_list(EvalRules, 1, BestLocal),
  theory_loop_N_aux(PT, Secondary, NoClauses, NP, BestTail),
  append(EvalRules, NP, NewPopulation),
  append(BestLocal, BestTail, Best).
theory_loop_N_aux([_PH | PT], Secondary, NoClauses, NewPopulation, Best) :-
  theory_loop_N_aux(PT, Secondary, NoClauses, NewPopulation, Best).
  
record_metrics_or([]) :-!.
record_metrics_or([hyp(_, ProbValues, _, PAcc, RMSE, _) | T]) :-
  record_rmse_or(RMSE),
  record_pacc_or(PAcc),
  record_pvalues_or(ProbValues),
  record_metrics_or(T).
  
record_rmse_or(RMSE) :-
  check_skill_option(record_rmse_or, File),
  (
    File == no ->
    true
  ;
    open(File, append, S),
    write(S, RMSE), write(S, '\n'),
    close(S)
  ).
  
record_pacc_or(PAcc) :-
  check_skill_option(record_pacc_or, File),
  (
    File == no ->
    true
  ;
    open(File, append, S),
    write(S, PAcc), write(S, '\n'),
    close(S)
  ).
  
record_pvalues_or(ProbValues) :-
  check_skill_option(record_pvalues_or, File),
  (
    File == no ->
    true
  ;
    open(File, append, S),
    write_to_csv(S, ProbValues),
    close(S)
  ).
  
%produces combinations of rule against remaining population
% fix hypothesis coverage later
iterative_evaluation(_, [], []) :-!.
% if you comment this you will get the problog error
iterative_evaluation(hyp(ThisHyp, ThisPValues, ThisDiff, ThisPAcc, ThisRMSE, ThisTAcc), [hyp([OtherHyp], _, _, _, _, _) | OtherT], List) :-
  memberchk(OtherHyp, ThisHyp), !,
  iterative_evaluation(hyp(ThisHyp, ThisPValues, ThisDiff, ThisPAcc, ThisRMSE, ThisTAcc), OtherT, List).
iterative_evaluation(FullHyp1, [FullHyp2 | OtherT], [ThisEval | Tail]) :-
  once(estimation_pruning(or, FullHyp1, FullHyp2)),
  FullHyp1 = hyp(Hyp1, _, _, _, _, _),
  FullHyp2 = hyp(Hyp2, _, _, _, _, _),
  append(Hyp1, Hyp2, ThisTheory),
  once(evaluation(ThisTheory, ThisEval)),
  iterative_evaluation(FullHyp1, OtherT, Tail).
iterative_evaluation(FullHyp1, [_|OtherT], Eval) :-
  iterative_evaluation(FullHyp1, OtherT, Eval).
