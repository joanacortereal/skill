:- use_module(library(lists)).
:- use_module(library(apply_macros), [maplist/3]).
:- use_module(library(varnumbers), [varnumbers/2]). % this is an undocumented YAP library. varnumbers/2 does the inverse of numbervars/3
:- use_module(library(system), [sleep/1]).
:- use_module(library(random)).
:- use_module(library(tries)).
:- use_module(library(terms)).

  
correct_format([], []).
correct_format([H | T], [hyp([H], _, _, _, _, _) | Tail]) :-
  correct_format(T, Tail).

my_varnumbers(In, Out) :-
  check_skill_option(varnumbers_file, File),
  tell(File), writeq(In), nl, told, %unnumbervars
  open(File, 'read', Stream), read(Stream, Out), close(Stream),
  delete_file(File), !.

prepare_stochastic_component :-
  check_skill_option(random_seed, Seed),
  Seed = rand(N1, N2, N3), !,
  setrand(rand(N1, N2, N3)).
prepare_stochastic_component :-
  datime(datime(_Years, _Months, _Days, Hours, Minutes, Seconds)),
  setrand(rand(Hours, Minutes, Seconds)).

decompose_clause(Clause, Head, Body, Args, Name, Arity) :-
  Clause = :-(Head, Body),
  Head =.. [Name|Args],
  length(Args, Arity), !.

save_state(Name, Size, Value) :-
  Value = ValueAux,
  numbervars(ValueAux, 0, _),
  check_skill_option(state_file, F),
  open(F, 'append', File),
  write(File, Name), write(File, '('), write(File, Size), write(File, ', '), writeq(File, ValueAux), write(File, ').\n'),
  close(File), fail.
save_state(_, _, _).

greater_than(Atom1, Atom2) :-
  name(Atom1, Codes1),
  name(Atom2, Codes2),
  greater_than_aux(Codes1, Codes2).
  
greater_than_aux([H1 | _T1], [H2 | _T2]) :-
  H1 > H2, !.
greater_than_aux([H | T1], [H | T2]) :-
  !, greater_than_aux(T1, T2).    

literals2clause([Head], Head):-
  functor(Head, Functor, _NumArgs), Functor\=(:-), !.
literals2clause([Head|BodyLiterals], (Head:-Body)):-
  !, list2Body(BodyLiterals, Body).
  
list2Body([], true):-!.
list2Body([A,B|C], (A,R)):-
  !, list2Body([B|C], R).
list2Body([A], A):-!.

n_best_rules_from_list(List, Best) :-
  n_best_rules_from_list(List, 1, Best).

n_best_rules_from_list(List, Size, Best) :-
  check_skill_option(evaluation, Evaluation),
  n_best_rules_from_list(Evaluation, List, Size, Best).

n_best_rules_from_list(_, [], _, []) :-!.
n_best_rules_from_list(_, _, 0, []) :-!.
n_best_rules_from_list(random, List, Size, Selection) :-
  select_random_from_list(List, Size, Selection).
n_best_rules_from_list(Evaluation, List, Size, [Best | BestList]) :-
  n_best_rules_from_list_aux(Evaluation, List, _, 1.00, Best),
  delete(List, Best, Rest),
  NewSize is Size - 1,
  n_best_rules_from_list(Evaluation, Rest, NewSize, BestList).
  
n_best_rules_from_list_aux(pacc, [], Best, _, Best) :-!.
n_best_rules_from_list_aux(pacc, [hyp(H, ProbValues, Diff, Acc, RMSE, TAcc) | T], _CurRule, CurAcc, Best) :-
  Acc < CurAcc, !,
  n_best_rules_from_list_aux(pacc, T, hyp(H, ProbValues, Diff, Acc, RMSE, TAcc), Acc, Best).
n_best_rules_from_list_aux(pacc, [hyp(H, ProbValues, Diff, Acc, RMSE, TAcc) | T], CurRule, CurAcc, Best) :-
  Acc == CurAcc, !,
  simplest_hyp(hyp(H, ProbValues, Diff, Acc, RMSE, TAcc), CurRule, SimplestHyp),
  n_best_rules_from_list_aux(pacc, T, SimplestHyp, CurAcc, Best).
n_best_rules_from_list_aux(pacc, [_H | T], CurRule, CurAcc, Best) :-
  n_best_rules_from_list_aux(pacc, T, CurRule, CurAcc, Best).
  
n_best_rules_from_list_aux(rmse, [], Best, _, Best) :-!.
n_best_rules_from_list_aux(rmse, [hyp(H, ProbValues, Diff, Acc, RMSE, TAcc) | T], _CurRule, CurRMSE, Best) :-
  RMSE < CurRMSE, !,
  n_best_rules_from_list_aux(rmse, T, hyp(H, ProbValues, Diff, Acc, RMSE, TAcc), RMSE, Best).
n_best_rules_from_list_aux(rmse, [hyp(H, ProbValues, Diff, Acc, RMSE, TAcc) | T], CurRule, CurRMSE, Best) :-
  RMSE == CurRMSE, !,
  simplest_hyp(hyp(H, ProbValues, Diff, Acc, RMSE, TAcc), CurRule, SimplestHyp),
  n_best_rules_from_list_aux(rmse, T, SimplestHyp, CurRMSE, Best).
n_best_rules_from_list_aux(rmse, [_H | T], CurRule, CurRMSE, Best) :-
  n_best_rules_from_list_aux(rmse, T, CurRule, CurRMSE, Best).

select_random_from_list(List, NoElements, Selection) :-
  length(List, N),
  (
    NoElements > N ->
    NewNoElements is N
  ;
    NewNoElements = NoElements
  ),
  randseq(NewNoElements, N, Positions),
  select_elements(List, Positions, Selection).
  
fix_randseq([], _, []) :-!.
fix_randseq([OldH | OldT], N, [NewH | NewT]) :-
  NewH is OldH + N,
  fix_randseq(OldT, N, NewT).

select_elements(_, [], []) :-!.
select_elements(List, [Position | Positions], [Element | Elements]) :-
  nth1(Position, List, Element),
  select_elements(List, Positions, Elements).
  
  
rule_value(Rule, Value) :-
  check_skill_option(evaluation, Evaluation),
  rule_value(Evaluation, Rule, Value).
rule_value(pacc, hyp(_, _, _, Value, _, _), Value).
rule_value(rmse, hyp(_, _, _, _, Value, _), Value).
rule_value(pacc, Rule, Value) :-
  hyp(Rule, _, _, Value, _, _).
rule_value(rmse, Rule, Value) :-
  hyp(Rule, _, _, _, Value, _).
 
 
simplest_hyp(hyp(H1, PV1, Diff1, PAcc1, RMSE1, TAcc1), hyp(H2, _PV2, _Diff2, _PAcc2, _RMSE2, _TAcc2), hyp(H1, PV1, Diff1, PAcc1, RMSE1, TAcc1)) :-
  length(H1, LenH1),
  length(H2, LenH2),
  LenH1 < LenH2, !.
simplest_hyp(hyp(H1, PV1, Diff1, PAcc1, RMSE1, TAcc1), hyp(H2, _PV2, _Diff2, _PAcc2, _RMSE2, _TAcc2), hyp(H1, PV1, Diff1, PAcc1, RMSE1, TAcc1)) :-
  length(H1, LenH1),
  length(H2, LenH2),
  LenH1 == LenH2,
  count_literals(H1, NoLitH1),
  count_literals(H2, NoLitH2),
  NoLitH1 < NoLitH2, !.
simplest_hyp(_H1, H2, H2).

count_literals([], 0) :-!.
count_literals([H | T], Total) :-
  literals2clause(Lits, H),
  length(Lits, NoLit),
  count_literals(T, AuxTotal),
  Total is NoLit + AuxTotal.

%% copy_theory(+ListOfRules, -Copy)
%%copies a theory into new and uninstantiated terms  
%copy_theory([], []).
%copy_theory([H|T], [Rule|Rules]) :-
  %varnumbers(H, Temp),
  %copy_term(Temp, Rule),
  %copy_theory(T, Rules).