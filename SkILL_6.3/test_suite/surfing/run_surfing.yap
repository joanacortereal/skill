z(EstA, EvA, EstO, EvO) :-
  yap_flag(verbose, silent),
  consult('/home/jcr/jcr-svn/trunk/SkILL_6.3/skill.yap'),
  %options
  set_skill_option(and_pruning_est, EstA),
  set_skill_option(and_pruning_eval, EvA),
  set_skill_option(or_pruning_est, EstO),
  set_skill_option(or_pruning_eval, EvO),
  set_skill_option(length_of_theory, 2),
  run_skill('/home/jcr/jcr-svn/datasets/surfing/surfing.pl'),
  halt.
z :-
  writeln('FAILED'),
  halt.