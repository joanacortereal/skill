#/bin/bash
# Not sure diff f1 f2 | wc -l is the best way to quantify the number of lines that differ in both files, but hopefully cpu and walltimes will never be the same and so there qill be at least 6 lines in diff's output

no_tests=9
no_tests_tried=0
no_tests_passed=0

######### NO PRUNING #########
((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, off, off, off)" &> output$no_tests_tried
line_count=`diff surfing/oooo skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OOOO test - see output file for more details"
  exit
fi

######### AND EST PRUNING #########
((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(soft, off, off, off)" &> output$no_tests_tried
line_count=`diff surfing/sooo skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed SOOO test - see output file for more details"
  exit
fi

((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(hard, off, off, off)" &> output$no_tests_tried
line_count=`diff surfing/hooo skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed HOOO test - see output file for more details"
  exit
fi

######### AND PRED PRUNING #########
((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, soft, off, off)" &> output$no_tests_tried
line_count=`diff surfing/osoo skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OSOO test - see output file for more details"
  exit
fi

((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, hard, off, off)" &> output$no_tests_tried
line_count=`diff surfing/ohoo skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OHOO test - see output file for more details"
  exit
fi

######### OR EST PRUNING #########
((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, off, soft, off)" &> output$no_tests_tried
line_count=`diff surfing/ooso skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OOSO test - see output file for more details"
  exit
fi

((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, off, hard, off)" &> output$no_tests_tried
line_count=`diff surfing/ooho skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OOHO test - see output file for more details"
  exit
fi

######### OR PRED PRUNING #########
((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, off, off, soft)" &> output$no_tests_tried
line_count=`diff surfing/ooos skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OOOS test - see output file for more details"
  exit
fi

((no_tests_tried++))
yap -s 100000 -l surfing/run_surfing.yap -z "z(off, off, off, hard)" &> output$no_tests_tried
line_count=`diff surfing/oooh skill_state.yap | wc -l`
if [ $line_count -le "6" ];
then
  ((no_tests_passed++))
  rm skill_state.yap
  rm output$no_tests_tried
  echo "Passed test $no_tests_tried/$no_tests"
else
  echo "Failed OOOH test - see output file for more details"
  exit
fi

echo "OK"

