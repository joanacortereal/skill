:- consult('utils.yap').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ESTIMATION PRUNING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

estimation_pruning(and, _Theory1, _Theory2) :-
  check_skill_option(and_pruning_est, off), !.
estimation_pruning(and, Theory1, Theory2) :-
  check_skill_option(and_pruning_est, Pruning),
  estimate_theory(and, Theory1, Theory2, ResTheory),
  is_theory_kept(and, Pruning, ResTheory).
estimation_pruning(and, _, _) :-
  get_value(est_counter_and, N),
  writeln(estimation_pruning_and(N)),
  NewN is N + 1,
  set_value(est_counter_and, NewN), !,
  fail.
  
estimation_pruning(or, _Theory1, _Theory2) :-
  check_skill_option(or_pruning_est, off), !.
estimation_pruning(or, Theory1, Theory2) :-
  check_skill_option(or_pruning_est, Pruning),
  estimate_theory(or, Theory1, Theory2, ResTheory),
  is_theory_kept(or, Pruning, ResTheory).
estimation_pruning(or, _, _) :-
  get_value(est_counter_or, N),
  writeln(estimation_pruning_or(N)),
  NewN is N + 1,
  set_value(est_counter_or, NewN), !,
  fail.
  
%suceeds if theory should be kept  
is_theory_kept(_, off, _).
  
is_theory_kept(and, soft, hyp(_, _, Diff, _, _, _)) :-
  Diff >= 0.
  
is_theory_kept(and, hard, hyp(_, PValues, _, _, _, _)) :-
  exp_values(ExpValues),
  is_theory_kept_loop(and, hard, PValues, ExpValues).

is_theory_kept(and, safe, hyp(_, PValues, _, _, _, _)) :-
    exp_values(ExpValues),
    is_theory_kept_loop(and, safe, PValues, ExpValues).

is_theory_kept(or, soft, hyp(_, _, Diff, _, _, _)) :-
  Diff =< 0.
  
is_theory_kept(or, hard, hyp(_, PValues, _, _, _, _)) :-
  exp_values(ExpValues),
  is_theory_kept_loop(or, hard, PValues, ExpValues).

is_theory_kept(or, safe, hyp(_, PValues, _, _, _, _)) :-
    exp_values(ExpValues),
    is_theory_kept_loop(or, safe, PValues, ExpValues).

is_theory_kept_loop(and, hard, [], []) :- !.
is_theory_kept_loop(and, hard, [PVH | PVT], [EVH | EVT]) :-
  PVH >= EVH,
  is_theory_kept_loop(and, hard, PVT, EVT).

is_theory_kept_loop(and, safe, [], []) :- !, fail.
is_theory_kept_loop(and, safe, [PVH | PVT], [EVH | EVT]) :-
    PVH < EVH, !,
    is_theory_kept_loop(and, safe, PVT, EVT).
is_theory_kept_loop(and, safe, _, _).

is_theory_kept_loop(or, hard, [], []) :- !.
is_theory_kept_loop(or, hard, [PVH | PVT], [EVH | EVT]) :-
  PVH =< EVH,
  is_theory_kept_loop(or, hard, PVT, EVT).

is_theory_kept_loop(or, safe, [], []) :- !, fail.
is_theory_kept_loop(or, safe, [PVH | PVT], [EVH | EVT]) :-
    PVH > EVH, !,
    is_theory_kept_loop(or, safe, PVT, EVT).
is_theory_kept_loop(or, safe, _, _).

merge_theories(and, [Hyp1], [Hyp2], [ResHyp]) :-
  decompose_clause(Hyp1, Head, Body1, _, _, _),
  decompose_clause(Hyp2, _, Body2, _, _, _),
  ResHyp = (Head :- Body1, Body2).
  
merge_theories(or, Hyp1, Hyp2, ResHyp) :-
  append(Hyp1, Hyp2, ResHyp).

estimate_theory(Operation, hyp(Hyp1, PValues1, _, _, _, _), hyp(Hyp2, PValues2, _, _, _, _), hyp(ResHyp, ResPValues, Diff, PAcc, RMSE, TAcc)) :-
  check_skill_option(estimator, Estimator),
  estimate_theory_loop(Estimator, Operation, PValues1, PValues2, ResPValues),
  merge_theories(Operation, Hyp1, Hyp2, ResHyp),
  exp_values(ExpValues),
  examples(Examples),
  difference(ExpValues, ResPValues, Diff),
  predictive_accuracy(ExpValues, ResPValues, PAcc),
  root_mean_square_error(ExpValues, ResPValues, RMSE),
  threshold_accuracy(Examples, ResPValues, TAcc).
  
estimate_theory_loop(_Estimator, _Operation, [], [], []) :-!.
estimate_theory_loop(Estimator, Operation, [Theory1H | Theory1T], [Theory2H | Theory2T], [ResTheoryH | ResTheoryT]) :-
  estimate_point(Estimator, Operation, Theory1H, Theory2H, ResTheoryH),
  estimate_theory_loop(Estimator, Operation, Theory1T, Theory2T, ResTheoryT).

estimate_point(center, and, Point1, Point2, Result) :-
  Min is 0,
  Max is min(Point1, Point2),
  Result is (Min + Max) / 2.
  
estimate_point(center, or, Point1, Point2, Result) :-
  Min is min(Point1, Point2),
  MaxAux is Point1 + Point2,
  Max is min(MaxAux, 1),
  Result is (Min + Max) / 2.

estimate_point(independence, and, Point1, Point2, Result) :-
  Result is Point1 * Point2.

estimate_point(independence, or, Point1, Point2, Result) :-
  ResultAux is Point1 + Point2 - (Point1 * Point2),
  Result is min(ResultAux, 1).
  
estimate_point(max, and, Point1, Point2, Result) :-
  Result is min(Point1, Point2).

estimate_point(max, or, Point1, Point2, Result) :-
  ResultAux is Point1 + Point2,
  Result is min(ResultAux, 1).
  
estimate_point(min, and, Point1, Point2, Result) :-
  Dif is Point1 + Point2 - 1,
  Result is max(Dif, 0).  

estimate_point(min, or, Point1, Point2, Result) :-
  Result is min(Point1, Point2).
  
estimate_point(exclusion, and, Point1, Point2, Result) :-
  estimate_point(min, and, Point1, Point2, Result).
  
estimate_point(exclusion, or, Point1, Point2, Result) :-
  estimate_point(max, or, Point1, Point2, Result).
  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREDICTION PRUNING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prediction_pruning(_, [], []) :-!.
prediction_pruning(Operation, [TheoryInH | TheoryInT], [TheoryInH | Theories]) :-
  prediction_pruning(Operation, TheoryInH), !,
  prediction_pruning(Operation, TheoryInT, Theories).
prediction_pruning(Operation, [_ | TheoryInT], Theories) :-
  prediction_pruning(Operation, TheoryInT, Theories).
  
prediction_pruning(and, Theory) :-
  check_skill_option(and_pruning_eval, Pruning),
  is_theory_kept(and, Pruning, Theory).
prediction_pruning(and, _) :-
  get_value(eval_counter_and, N),
  writeln(prediction_pruning_and(N)),
  NewN is N + 1,
  set_value(eval_counter_and, NewN), !,
  fail.
  
prediction_pruning(or, Theory) :-
  check_skill_option(or_pruning_eval, Pruning),
  is_theory_kept(or, Pruning, Theory).
prediction_pruning(or, _) :-
  get_value(eval_counter_or, N),
  writeln(prediction_pruning_or(N)),
  NewN is N + 1,
  set_value(eval_counter_or, NewN), !,
  fail.
  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FITNESS PRUNING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fitness_pruning(and, primary, Theories, PrunedTheories) :-
  check_skill_option(and_primary_size, PSize),
  check_skill_option(and_primary_rank, PRank),
  n_best_rules_from_list(PRank, Theories, PSize, PrunedTheories).
  
fitness_pruning(and, secondary, Theories, PrunedTheories) :-
  check_skill_option(and_secondary_size, SSize),
  check_skill_option(and_secondary_rank, SRank),
  n_best_rules_from_list(SRank, Theories, SSize, PrunedTheories).
