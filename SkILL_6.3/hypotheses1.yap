%:- consult('../MetaProbLog/metaproblog.yap').
:- consult('../GILPS_6.3/gilps.pl').
:- consult('utils.yap').

:- use_module(library(rbtrees), [rb_new/1, rb_lookup/3, rb_insert/4, rb_update/4, rb_keys/2, rb_visit/2, rb_map/3, rb_size/2]).

% hypotheses_size_one(+File, -[Hypotheses])
% finds hypotheses of one clause only from examples in File using a top theory
hypotheses_size_one(File, ListOfHyps1) :-
  trie_open(Trie),
  assert(skill_trie(Trie)),
  set(verbose, 0),
  read_problem(File), % get mode declarations using GILPS
  build_theory(HypGenInfo), % modified original GILPS predicate to get hypotheses generated from examples using top theory
  rb_visit(HypGenInfo, HypGenInfoList), % retrieve information in red-black tree used in GILPS
  build_list_of_all_hyps(HypGenInfoList, ListOfHypsFull),
  remove_duplicate_hyps(ListOfHypsFull, ListOfHyps1Aux),
  rename_vars(ListOfHyps1Aux, ListOfHyps1),
  %correct_format(ListOfHyps1Aux, ListOfHyps1),
  %ListOfHyps1Aux = ListOfHyps1,
  %writeln(ListOfHyps1), break,
  length(ListOfHypsFull, AllHyps1),
  length(ListOfHyps1, UniqueHyps1),
  writeln(removed_permutations(UniqueHyps1/AllHyps1)),
  abolish_prob_predicates,
  consult_prob_facts.
  
rename_vars([], []) :- !.
rename_vars([H | T], [NewH | NewT]) :-
  my_varnumbers(H, NewH),
  numbervars(NewH, 0, _),
  rename_vars(T, NewT).
  
abolish_prob_predicates :-
  check_skill_option(prob_predicates, ProbPreds),
  abolish_prob_predicates(ProbPreds).
abolish_prob_predicates([]) :- !.
abolish_prob_predicates([H | T]) :-
  abolish(H),
  abolish_prob_predicates(T).

consult_prob_facts :-
  check_skill_option(prob_background, ProbBack),
  consult_prob_facts(ProbBack).
consult_prob_facts([]) :- !.
consult_prob_facts([H | T]) :-
  consult(H),
  consult_prob_facts(T).

  
build_list_of_all_hyps([], []) :-!.
build_list_of_all_hyps([Hyp-_ | HypListT], [ListOfRulesH | ListOfRulesT]):-
  length(Hyp, L),
  L > 1, !,
  reverse(Hyp, HypAux),
  literals2clause(HypAux, ListOfRulesH),
  build_list_of_all_hyps(HypListT, ListOfRulesT).
build_list_of_all_hyps([_HypH | HypT], ListOfRules) :-
  build_list_of_all_hyps(HypT, ListOfRules).
  
% remove_duplicate_hyps(+[Hypotheses], -[UniqueHyps])
% uses a trie to remove duplicate hypotheses
% does this by ordering literals alpahbetically and inserting them in the trie
remove_duplicate_hyps(Duplicates, Rules) :-
  skill_trie(Trie),
  remove_duplicate_hyps_aux(Trie, Duplicates, References),
  convert_references_to_terms(Trie, References, Rules).
  
remove_duplicate_hyps_aux(_, [], []) :-!.
remove_duplicate_hyps_aux(Trie, [Rule | List], References) :-
  decompose_clause(Rule, Head, BodyLit, _Args, _Name, _Arity),
  list2Body(Body, BodyLit),
  quicksort(Body, NormalizedBody),
  append([Head], NormalizedBody, NormalizedClauseAux),
  copy_term(NormalizedClauseAux, NormalizedClause),
  numbervars(NormalizedClause, 0, _),
  (
    trie_check_entry(Trie, NormalizedClause, _Reference) -> !,
    remove_duplicate_hyps_aux(Trie, List, References)
    ;
    trie_put_entry(Trie, NormalizedClause, Reference),
    References = [Reference | OtherRefs],
    remove_duplicate_hyps_aux(Trie, List, OtherRefs)
  ).
  
convert_references_to_terms(_, [], []) :-!.
convert_references_to_terms(Trie, [RefH | RefT], [RuleH | RuleT]) :-
  trie_get_entry(RefH, Entry),
  literals2clause(Entry, RuleH),
  convert_references_to_terms(Trie, RefT, RuleT).

quicksort([X|Xs],Ys) :-
  partition(Xs,X,Left,Right),
  quicksort(Left,Ls),
  quicksort(Right,Rs),
  append(Ls,[X|Rs],Ys).
quicksort([],[]).

partition([X|Xs],Y,Ls,[X|Rs]) :-
  functor(X, NameX, _),
  functor(Y, NameY, _),
  greater_than(NameX, NameY), !,
  partition(Xs,Y,Ls,Rs).
partition([X|Xs],Y,[X|Ls],Rs) :-
  partition(Xs,Y,Ls,Rs).
partition([],_Y,[],[]).