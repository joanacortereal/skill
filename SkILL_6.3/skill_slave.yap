:- consult('evaluation').
:- consult('utils.yap').
:- use_module(library(lists)).
:- use_module(library(terms)).
:- use_module(library(system)).
  
% evaluation_server(+Port, +File)
% this is a process which loops evaluating hyptheses it receives through a socket connection in Port
evaluation_server(Port, Files) :-
  writeln('I am skill slave'),
  consult(Files),
  %consult('/home/jcr/jcr-svn/trunk/datasets/metabolism/bootstrap100/train1'),
  jcr_open_server_socket(Port, Socket),
  assert_example_data,
  sleep(0.01),
  evaluation_loop(Socket).
  
evaluation_loop(Socket) :-
  jcr_read_from_socket(Socket, query(Hyp)),
  (
    real_evaluation(Hyp, EvalHyp) -> !,
    jcr_write_to_socket(Socket, result(EvalHyp))
  ;
    jcr_write_to_socket(Socket, failed)
  ),
  evaluation_loop(Socket).
evaluation_loop(_) :-
  writeln('EVALUATION FAILED'),
  halt.
  
check_skill_option(_, _) :- fail.
