:- discontiguous type_check:(:-)/1.
:- yap_flag(informational_messages, off).
:- yap_flag(redefine_warnings, off).
:- yap_flag(single_var_warnings, off).
:- use_module(library(system)).
:- use_module(library(lists)).
:- yap_flag(unknown,fail).
:- yap_flag(informational_messages, off).

%multifile this
user:term_expansion(prob_example(Ex, Weight, Prob), [example(Ex, Weight), prob_example(Ex, Weight, Prob)]).

run_skill(File) :-
  run_skill(File, _).
run_skill(File, BestHyp) :-
  prepare_state,
  (
  skill_option(parallel, yes) ->
  consult('/home/jcr/jcr-svn/trunk/SkILL_6.3/engine_par.yap'),
  run_skill_par(File, BestHyp)
  ;
  consult('/home/jcr/jcr-svn/trunk/SkILL_6.3/engine_seq.yap'),
  run_skill_seq(File, BestHyp)
  ).

  
skill_list_of_options(parallel, [yes, no]). % chooses if the execution is concurrent - work in progress
skill_list_of_options(and_primary_rank, [pacc, rmse, random]). % ranking metric for primary in AND search space
skill_list_of_options(and_secondary_rank, [pacc, rmse, random]). % ranking metric for secondary in AND search space
skill_list_of_options(and_primary_size, number). % no. of members of primary in AND search space
skill_list_of_options(and_secondary_size, number). % no. of members of secondary in AND search space
skill_list_of_options(or_primary_rank, [pacc, rmse, random]). % ranking metric for primary in OR search space
skill_list_of_options(or_secondary_rank, [pacc, rmse, random]). % ranking metric for secondary in OR search space
skill_list_of_options(or_primary_size, number). % no. of members of primary in OR search space
skill_list_of_options(or_secondary_size, number). % no. of members of secondary in OR search space
skill_list_of_options(evaluation, [pacc, rmse]). % evaluation metric
skill_list_of_options(and_pruning_est, [soft, hard, safe, off]). % estimation pruning option for conjunction
skill_list_of_options(and_pruning_eval, [soft, hard, safe, off]). % evaluation pruning option for conjunction
skill_list_of_options(or_pruning_est, [soft, hard, off]). % estimation pruning option for disjunction
skill_list_of_options(or_pruning_eval, [soft, hard, off]). % evaluation pruning option for disjunction
skill_list_of_options(length_of_theory, number). % maximum no. of clauses in a theory
skill_list_of_options(facts, list). % paths to file containing the PBK
skill_list_of_options(dummy_evaluation, [yes, no]). % use problog or random number generator
skill_list_of_options(search, [exhaustive, greedy, smart]). % choose between primary and secondary or greedy search
skill_list_of_options(estimator, [center, independence, max, min, exclusion]). % estimator to be used instead of exact evaluation
skill_list_of_options(remote_evaluation, [no, yes]). % performs evaluation on a slave program which can be rebooted during runtime
skill_list_of_options(socket, number). % Socket for remote evaluation
skill_list_of_options(socket_count, number). % Number of remote evaluations a socket process performed.
skill_list_of_options(socket_reboot, number). % Number of consecutive evaluations done by a remote process before creating a new one
skill_list_of_options(socket_port, number). % Port number for socket in remote evaluation
skill_list_of_options(socket_port_increment, number). % Increment in the number of the port when a socket fails
skill_list_of_options(state_file, atom). % file where skill saves its state. It is written over on every execution
skill_list_of_options(test_estimators, [no, yes]). % creates files containing estimation values using all estimators. Filesnames start with the name of the state_file
skill_list_of_options(tune_set, atom). % uses a tuning set to choose the best hypothesis
skill_list_of_options(test_set, atom). % uses a test set to evaluate the final hypothesis
skill_list_of_options(smart_strategy, [distance, threshold]). % select the smart strategy complementary values comparison metric
skill_list_of_options(complementary_threshold, number). % sets the maximum threshold that a theory can differ from the complementary values
skill_list_of_options(random_seed, rand_term). % sets the random seed for the stochastic execution of the program
skill_list_of_options(varnumbers_file, atom). % sets the name of the auxiliary file that is used to perform varnumbers of a clause
skill_list_of_options(prob_predicates, list). % list of probabilistic predicates in the background knowledge
skill_list_of_options(prob_background, list). % list of files where the probabilistic background is contained
skill_list_of_options(bdd_timeout, number). % sets the bdd timeout value in seconds
skill_list_of_options(record_rmse_and, atom). % saves the RMSE values of all rules to the file given in atom
skill_list_of_options(record_rmse_or, atom). % saves the RMSE values of all theories of size > 1 to the file given in atom
skill_list_of_options(record_pacc_and, atom). % saves the PAcc values of all rules to the file given in atom
skill_list_of_options(record_pacc_or, atom). % saves the PAcc values of all theories of size > 1 to the file given in atom
skill_list_of_options(record_pvalues_and, atom). % saves the Log Likelihood values of all rules to the file given in atom
skill_list_of_options(record_pvalues_or, atom). % saves the Log Likelihood values of all theories of size > 1 to the file given in atom
skill_list_of_options(timeout, number). % time in seconds after which the execution ends. Default value is -1 for no timeout

default(skill_option(parallel, no)).
default(skill_option(and_primary_rank, pacc)).
default(skill_option(and_secondary_rank, random)).
default(skill_option(and_primary_size, 20)).
default(skill_option(and_secondary_size, 200)).
default(skill_option(or_primary_rank, pacc)).
default(skill_option(or_secondary_rank, random)).
default(skill_option(or_primary_size, 20)).
default(skill_option(or_secondary_size, 200)).
default(skill_option(evaluation, pacc)).
default(skill_option(and_pruning_est, soft)).
default(skill_option(and_pruning_eval, soft)).
default(skill_option(or_pruning_est, soft)).
default(skill_option(or_pruning_eval, soft)).
default(skill_option(length_of_theory, 3)).
default(skill_option(facts, [])).
default(skill_option(dummy_evaluation, no)).
default(skill_option(search, exhaustive)).
default(skill_option(estimator, center)).
default(skill_option(remote_evaluation, no)).
default(skill_option(socket, -1)).
default(skill_option(socket_count, 0)).
default(skill_option(socket_reboot, 100000)).
default(skill_option(socket_port, 5000)).
default(skill_option(socket_port_increment, 1)).
default(skill_option(state_file, 'skill_state.yap')).
default(skill_option(test_estimators, no)).
default(skill_option(tune_set, no)).
default(skill_option(test_set, no)).
default(skill_option(smart_strategy, distance)).
default(skill_option(complementary_threshold, 0.1)).
default(skill_option(random_seed, rand(1, 2, 3))).
default(skill_option(varnumbers_file, aux)).
default(skill_option(prob_predicates, [])).
default(skill_option(prob_background, [])).
default(skill_option(bdd_timeout, 10)).
default(skill_option(record_rmse_and, no)).
default(skill_option(record_rmse_or, no)).
default(skill_option(record_pacc_and, no)).
default(skill_option(record_pacc_or, no)).
default(skill_option(record_pvalues_and, no)).
default(skill_option(record_pvalues_or, no)).
default(skill_option(timeout, -1)).


set_skill_option(A, B) :-
  valid_option(A, B),
  skill_option(A, X), !,
  retract(skill_option(A, X)),
  assert(skill_option(A, B)).
set_skill_option(A, B) :-
  valid_option(A, B), !,
  assert(skill_option(A, B)).
set_skill_option(A, B) :-
  write('error: option '), write(skill_option(A, B)), writeln(' is not valid').
  
valid_option(A, B) :-
  skill_list_of_options(A, List), !,
  valid_option_aux(List, B).
  
valid_option_aux(number, B) :-
  number(B).
valid_option_aux(atom, B) :-
  atom(B).
valid_option_aux(list, B) :-
  is_list(B).
valid_option_aux(rand_term, rand(_, _, _)).
valid_option_aux(rand_term, none).
valid_option_aux(List, B) :-
  is_list(List),
  memberchk(B, List).
  
prepare_state :-
  file_exists('skill_state.yap'), !,
  delete_file('skill_state.yap').
prepare_state.

check_skill_option(X, Res):-
  skill_option(X, Res), !.
check_skill_option(X, Res) :-
  default(skill_option(X, Res)).
  