:- consult('../MapReduceForProlog/MapReduce/main.yap').
:- consult('../MapReduceForProlog/MapReduce/aux.yap').

:- consult('evaluation.yap').
:- consult('hypotheses1.yap').
:- consult('utils.yap').
:- consult('population.yap').

:- use_module(library(system)).
:- use_module(library(lists)).
:- use_module(library(hacks)).

%user:term_expansion(prob_example(Ex, Weight, Prob), [example(Ex, Weight), prob_example(Ex, Weight, Prob)]).

% run_skill_par(+File)
% runs skill solving the problem in File
% uses toplog in GILPS to find hypotheses of one clause
run_skill_par(File) :-
  mutex_create(temp_mutex),
  hypotheses_size_one(File, ListOfHyps1), !,
  save_state(population, 1, ListOfHyps1),
  construct_theory_par(ListOfHyps1, [hyp(Hyp, _, _, PAcc, RMSE, TAcc)]),
  writeln('------- RESULT -------'),
  writeln(hypotheses(Hyp)),
  writeln(pacc(PAcc)),
  writeln(rmse(RMSE)),
  writeln(tacc(TAcc)).
  
%%%%%%%%%%%%%%%%%%%%% THEORY LOOP OVER HYP SPACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% construct_theory_par (+[Hyps1], -BestEvaluatedHyp)
% loops over all theory sizes to fing the best hypothesis
construct_theory_par(ListOfHyps1, BestHyp) :-
  % user options
  length_of_theory(LengthOfTheory),
  evaluation(Evaluation),
  %threshold(Threshold),
  pruning(Pruning),
  criterion(Criterion),
  primary(PrimarySize),
  secondary(SecondarySize),
  init_interface,
  %stack_dump,
  add_brackets(ListOfHyps1, ListOfHyps1B),
  create_communicator('c1', [team(0, 1)], ListOfHyps1B),
  %sleep(10),
  theory_loop(ListOfHyps1, LengthOfTheory, Evaluation, Pruning, Criterion, PrimarySize, SecondarySize, BestHypsList),
  delete_communicator('c1'),
  end_interface,
  n_best_rules_from_list(Evaluation, BestHypsList, 1, BestHyp).

theory_loop(ListOfHyps1, LengthOfTheory, Evaluation, Pruning, Criterion, PrimarySize, SecondarySize, BestHypsList):-
  length(ListOfHyps1, N),
  Max is N-1,
  evaluation(Evaluation),
  facts(File),
  writeln(before_mr),
  map_reduce_call('c1', skill_map, skill_reduce, [1-Max], (create_evaluation_processes('c1', File))),
  writeln(after_mr),
  map_reduce_get_result('c1', result(BestHyps1, EvalHyps1), (close_static_array('c1'))),
  save_state(evaluated, 1, EvalHyps1),
  theory_loop_N(Pruning, Criterion, EvalHyps1, EvalHyps1, PrimarySize, SecondarySize, 2, LengthOfTheory, BestHypsN),
  append([BestHyps1], BestHypsN, BestHypsList).
  
add_brackets([], []) :-!.
add_brackets([H | T], [[H] | OT]) :-
  add_brackets(T, OT).
  
%loops until size of theory is reached
%include threshold later!
theory_loop_N(_, _, _, _, _, _, _, 1, []) :- !.
theory_loop_N(_Pruning, _Criterion, _PrimaryPopulation, _SecondaryPopulation, _PrimarySize, _SecondarySize, NoClauses, LengthOfTheory, []) :-
  NoClauses > LengthOfTheory, !.
theory_loop_N(Pruning, Criterion, PrimaryPopulation, SecondaryPopulation, PrimarySize, SecondarySize, NoClauses, LengthOfTheory, BestTheories) :-
  prune_search_space(Pruning, PrimaryPopulation, PrunedPrimaryPopulation),
  prune_search_space(Pruning, SecondaryPopulation, PrunedSecondaryPopulation),
  select_population(Criterion, PrunedPrimaryPopulation, PrunedSecondaryPopulation, PrimarySize, SecondarySize, Primary, Secondary),
  theory_loop_N_aux(Pruning, Primary, Secondary, NewPrimaryPopulation, BestLocalList),
  evaluation(Evaluation),
  n_best_rules_from_list(Evaluation, BestLocalList, 1, BestLocal),
  NewNoClauses is NoClauses + 1,
  theory_loop_N(Pruning, Criterion, NewPrimaryPopulation, Secondary, PrimarySize, SecondarySize, NewNoClauses, LengthOfTheory, BestTheoriesTail),
  append(BestLocal, BestTheoriesTail, BestTheories).
  
%iterates over primary population
theory_loop_N_aux(_Pruning, [], _, [], []) :-!.
theory_loop_N_aux(Pruning, [PH | PT], Secondary, NewPopulation, Best) :-
  prune_rule_space(Pruning, PH, Secondary, UsefulRules),
  length(Secondary, N),
  length(UsefulRules, M),
  writeln(pruned_rules(M/N)),
  generate_population(PH, UsefulRules, ListOfRules),
  ListOfRules = [H | _], length(H, Size),
  save_state(population, Size, ListOfRules),
  length(ListOfRules, NLR),
  Max is NLR-1,
  map_reduce_call('c1', skill_map, skill_reduce, [1-Max], data_from_file(ListOfRules, 'c1')),
  map_reduce_get_result('c1', result(BestLocal, EvalRules), (close_static_array('c1'))),
  save_state(evaluated, Size, EvalRules),
  theory_loop_N_aux(Pruning, PT, Secondary, NP, BestTail),
  append(EvalRules, NP, NewPopulation),
  append([BestLocal], BestTail, Best).
  
generate_population(_, [], []) :-!.
% if you comment this you will get the problog error
generate_population(hyp([ThisHyp], ThisPValues, ThisDiff, ThisPAcc, ThisRMSE, ThisTAcc), [hyp(Hyp, _PValues, _Diff, _PAcc, _RMSE, _TAcc) | OtherT], List) :-
  member(ThisHyp, Hyp),
  generate_population(hyp([ThisHyp], ThisPValues, ThisDiff, ThisPAcc, ThisRMSE, ThisTAcc), OtherT, List).
generate_population(hyp(Hyp1, _, _, _, _, _), [hyp(Hyp2, _, _, _, _, _) | OtherT], [ThisTheory | Tail]) :-
  append(Hyp1, Hyp2, ThisTheory),
  generate_population(hyp(Hyp1, _, _, _, _, _), OtherT, Tail).
  
%%%%%%%%%%%%%%%%%%%%%% REMOTE PREDICATES %%%%%%%%%%%%%%%%%%%%%%%%%%%
  
% create_evaluation_processes(+Communicator, +File)
% this is a temporary solution which creates as many processes as there are team members
% these processes will be performing the evaluation of the hypotheses
% File containes the probabilistic facts necessary for the evaluation of the hypotheses and must be loaded by the evaluation processes
create_evaluation_processes(Comm, File) :-
  team(Comm, TeamIDs),
  create_evaluation_processes_aux(TeamIDs, File),
  writeln(end_of_create).
  
create_evaluation_processes_aux([], _) :- writeln(again).
% for mpi
create_evaluation_processes_aux([H | T], File) :-
  %Port is 5025,
  mpi_comm_rank(Rank),
  Port is 5060 + Rank*100 + H,
  %Port is 5070 + H,
  % remember to change this!!
  atomic_concat(['/home/jcr/jcr-svn/trunk/yap-6.3/ARCH-laptop/yap -l /home/jcr/jcr-svn/trunk/SkILL/skill_slave.yap -g "evaluation_server(', Port, ',\'', File, '\')"  &>log_mesmo_estranho &'], Command),
  %%writeln(Command),
  %writeln(Command),
  system(Command),
  writeln(here),
  %start_low_level_trace,
  %free_mem,
  %jcr_new_system(Port, File),
  writeln(but_not_here),
  loop_connect_to_server(Port, ThisSocket, 10),
  assert(my_sockie(H, ThisSocket)),
  writeln(and_there),
  create_evaluation_processes_aux(T, File).
% if there is no mpi
create_evaluation_processes_aux([H | T], File) :-
  %Port is 5025,
  Port is 5000 + H,
  %Port is 5070 + H,
  % remember to change this!!
  atomic_concat(['/home/jcr/jcr-svn/trunk/yap-6.3/ARCH-DCC/yap -l /home/jcr/jcr-svn/trunk/SkILL/skill_slave.yap -g "evaluation_server(', Port, ',\'', File, '\')" &'], Command),
  %writeln(Command),
  system(Command),
  loop_connect_to_server(Port, ThisSocket, 10),
  assert(my_sockie(H, ThisSocket)),
  create_evaluation_processes_aux(T, File). 
  
loop_connect_to_server(_, _, 0) :-
  writeln('CANT CONNECT TO SOCKET'),
  halt.
loop_connect_to_server(Port, ThisSocket, N) :-
  sleep(0.5),
  jcr_connect_to_socket(Port, ThisSocket),
  nonvar(ThisSocket), !.
loop_connect_to_server(Port, ThisSocket, N) :-
  NewN is N-1,
  loop_connect_to_server(Port, ThisSocket, NewN).
  
% skill_map(+Hypothesis, -result(BestEvaluatedHyp, [AllEvaluatedHyps]))
% map predicate used by MapReduce which evaluated Hypothesis
% receives a hypothesis and returns its evaluation and a list containing it
% this is done so that reduce can return the best hypothesis from all the maps in a faster time
skill_map(Hyp, result(Term, [Term])) :-
  thread_self(ID),
  my_sockie(ID, Socket),
  socket_send(Socket, Hyp),
  socket_recv(Socket, Term).
skill_map(_, _) :-
  w('FAILING MAP!!!').
  
dummy_reduce(_, result(finished, dummy)).

% skill_reduce(+[EvaluatedHyps], -result(BestEvaluatedHyp, [AllEvaluatedHyps]))
% aggregates all hypotheses and their metrics in AllEvaluatedHyps
% also returns the best hypothesis BestEvaluatedHyp
skill_reduce(ListOfHyps, result(Best, ListOfRules)) :-
  evaluation(Evaluation),
  separate_result(Evaluation, ListOfHyps, hyp(_, _, _, 1.00, 1.00, 1.00), Best, ListOfRules).
skill_reduce(_, _):-
  w('FAILING REDUCE!!!').

separate_result(_, [], Best, Best, []).
separate_result(Evaluation, [result(ThisHyp, List) | Tail], CurrentHyp, Best, ListOfRules) :-
  get_value(Evaluation, ThisHyp, Value),
  get_value(Evaluation, CurrentHyp, CurrValue),
  (
    Value < CurrValue ->
    separate_result(Evaluation, Tail, ThisHyp, Best, NewListOfRules)
    ;
    separate_result(Evaluation, Tail, CurrentHyp, Best, NewListOfRules)
  ),
  append(List, NewListOfRules, ListOfRules).
  
get_value(pacc, hyp(_, _, _, PAcc, _, _), PAcc).
get_value(rmse, hyp(_, _, _, _, RMSE, _), RMSE).

socket_send(Socket, Hyp) :-
  %mutex_lock(temp_mutex_mpi),
  %term_to_atom(Hyp, HypAtom),
  %mutex_unlock(temp_mutex_mpi),
  Hyp = HypAtom,
  jcr_write_to_socket(Socket, HypAtom).
  
socket_recv(Socket, Term) :-
  Term = Atom,
  jcr_read_from_socket(Socket, Atom).
  %mutex_lock(temp_mutex_mpi),
  %atom_to_term(Atom, TermAux, _),
  %mutex_unlock(temp_mutex_mpi),
  %varnumbers(TermAux, Term).
  
