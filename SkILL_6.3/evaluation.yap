%:- use_module('../MetaProbLog/metaproblog.yap').
%:- use_module('ad_converter').
:- nb_setval(problog_steps, 0).
:- use_module(library(problog)).
:- use_module(library(random)).
:- use_module(library(varnumbers)).
%:- use_module(library(maplist)).
:- yap_flag(unknown, fail).

%:- consult('utils.yap').

% assert_example_data
% asserts properties of prob_example such as lists of all the queries and expected values so they are visible everywhere
assert_example_data :-
  set_value(eval_counter, 0),
  set_value(est_counter_and, 0),
  set_value(est_counter_or, 0),
  set_value(eval_counter_and, 0),
  set_value(eval_counter_or, 0),
  set_value(timeout_counter, 0),
  update_queries,
  assert(best(hyp(_, _, _, 1, 1, 1))),
  % set problog options
  dynamic(prob_inferece/0),
  check_skill_option(bdd_timeout, Timeout),
  set_problog_flag(bdd_time, Timeout),
  %set_problog_flag(sld_timeout, 2),
  %set_problog_flag(script_timeout, 2),
  %set_problog_flag(bdd_timeout, 2),
  %set_problog_flag(sampling_timeout, 15),
  %set_problog_flag(sampling_convergence_timeout, 10),
  %set_problog_flag(sample_interval, 100),
  %set_problog_flag(min_samples, 1000),
  %set_problog_flag(convergance_critirium, first_condition),
  (
    check_skill_option(remote_evaluation, yes) ->
    check_skill_option(socket_port, Port),
    check_skill_option(facts, Files),
    create_evaluation_process(Port, Files)
  ;
    true
  ),
  (
    check_skill_option(test_estimators, yes) -> !,
    prepare_estimations
  ;
    true
  ).
  
update_queries :-
  findall(prob_example(Query, Weight, ExpValue), prob_example(Query, Weight, ExpValue), Examples),
  assertz(examples(Examples)),
  findall(ExpValue, prob_example(_Query, _Weight, ExpValue), ExpValues),
  assertz(exp_values(ExpValues)),
  findall(Query, prob_example(Query, _Weight, _ExpValue), Queries),
  assertz(queries(Queries)).


%%%%%%%%%%%%%%%%%%% EVALUATION  %%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  
% evaluation(+[ListOfClauses], -Metrics)
% returns the metrics concerning a hypotheses
% syntactic sugar for dummy testing
evaluation(Hyp, Eval) :-
  check_skill_option(timeout, Seconds),
  (
    Seconds > 0 ->
      initial_time(TInit),
      statistics(walltime, [TNow, _]),
      Elapsed is (TNow - TInit)/1000000,
      (
	Elapsed > Seconds,
	save_state(execution_time, timeout, Elapsed),
	get_value(eval_counter, EvaluationCounter),
	get_value(est_counter_and, EstCounterAnd),
	get_value(est_counter_or, EstCounterOr),
	get_value(eval_counter_and, EvalCounterAnd),
	get_value(eval_counter_or, EvalCounterOr),
	get_value(timeout_counter, Timeout),
	save_state(eval_counter, timeout, EvaluationCounter),
	save_state(est_counter_and, timeout, EstCounterAnd),
	save_state(est_counter_or, timeout, EstCounterOr),
	save_state(eval_counter_and, timeout, EvalCounterAnd),
	save_state(eval_counter_or, timeout, EvalCounterOr),
	save_state(timeout_counter, timeout, Timeout),
	best(Best), numbervars(Best, 0, _),
	save_state(best, final, Best),
	writeln('TIMEOUT'),
	writeln(Best),
	halt
      ;
	true
      )
  ;
    true
  ),
  evaluation1(Hyp, Eval),
  rule_value(Eval, ThisMetric),
  best(Best),
  rule_value(Best, BestMetric),
  (
    ThisMetric < BestMetric ->
      abolish(best, 1),
      assert(best(Eval))
  ;
    true
  ).
evaluation1(HypAux, Eval) :-
  check_skill_option(dummy_evaluation, yes), !,
  once(my_varnumbers(HypAux, Hyp)),
  get_value(eval_counter, N),
  writeln(evaluation(N)),
  dummy_evaluation(Hyp, Eval),
  NewN is N + 1,
  set_value(eval_counter, NewN).
evaluation1(HypAux, Eval) :-
  check_skill_option(remote_evaluation, yes), !,
  once(my_varnumbers(HypAux, Hyp)),
  get_value(eval_counter, N),
  writeln(evaluation(N)),
  remote_evaluation(Hyp, Eval),
  NewN is N + 1,
  set_value(eval_counter, NewN).
evaluation1(HypAux, Eval) :-
  once(my_varnumbers(HypAux, Hyp)),
  get_value(eval_counter, N),
  writeln(evaluation(N)),
  real_evaluation(Hyp, Eval),
  NewN is N + 1,
  set_value(eval_counter, NewN).
  
remote_evaluation(Hyp, EvalHyp) :-
  check_skill_option(socket_count, SocketCount),
  check_skill_option(socket_reboot, SocketReboot),
  SocketCount < SocketReboot, !,
  check_skill_option(socket, Socket),
  jcr_write_to_socket(Socket, query(Hyp)),
  jcr_read_from_socket(Socket, Result),
  (
    Result = result(EvalHyp) ->
    NewSocketCount is SocketCount + 1,
    set_skill_option(socket_count, NewSocketCount)
  ;
    set_skill_option(socket_count, SocketReboot),
    !, fail
  ).
remote_evaluation(Hyp, EvalHyp) :-
  check_skill_option(socket, Socket),
  jcr_write_to_socket(Socket, die),
  set_skill_option(socket_count, 0),
  check_skill_option(socket_port, OldPort),
  check_skill_option(socket_port_increment, Inc),
  NewPort is OldPort + Inc,
  set_skill_option(socket_port, NewPort),
  check_skill_option(facts, Files),
  create_evaluation_process(NewPort, Files),
  check_skill_option(socket_count, NewSocketCount),
  check_skill_option(socket, NewSocket),
  jcr_write_to_socket(NewSocket, query(Hyp)),
  jcr_read_from_socket(NewSocket, result(EvalHyp)), !,
  NewCount is NewSocketCount + 1,
  set_skill_option(socket_count, NewCount).
  
  
% dummy_evaluation(+[ListOfClauses], -hyp([ListOfClauses], [ProbValues], Difference, ProbabilisticAccuracy, RootMeanSquareError, ThresholdAccuracy))
% can be used to emulate probabilistic evaluation of hypotheses
% generates a list of random numbers instead of calculating ProbValues
dummy_evaluation(Hyp, hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc)) :-
  examples(Examples),
  exp_values(ExpValues),
  queries(Queries),
  assert_disjunction(Hyp),
  assertz(prob_inferece),
  dummy_problog(Queries, ProbValues),
  retractall(prob_inferece),
  retract_disjunction(Hyp),
  difference(ExpValues, ProbValues, Diff),
  predictive_accuracy(ExpValues, ProbValues, PAcc),
  root_mean_square_error(ExpValues, ProbValues, RMSE),
  threshold_accuracy(Examples, ProbValues, TAcc).
  
dummy_problog([], []) :-!.
dummy_problog([_H|T], [Value|OT]) :-
  random(ValueAux),
  Value is ValueAux,
  dummy_problog(T, OT).
  
  
% real_evaluation(+[ListOfClauses], -hyp([ListOfClauses], [ProbValues], Difference, ProbabilisticAccuracy, RootMeanSquareError, ThresholdAccuracy))
% calculates probabilistic values of a hypotheses wrt examples and BK
% using ProbValues, calculates several evaluation metrics for the given hypothesis
% must assert the hypotheses in the database to evaluate it
% uses sampling if exact inference takes too long
real_evaluation(Hyp, hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc)) :-
  copy_term(Hyp, HypAux),
  numbervars(HypAux, 0, _),
  open('off', 'append', F),
  writeln(F, HypAux),
  close(F),
  examples(Examples),
  exp_values(ExpValues),
  queries(Queries),
  once(assert_disjunction(Hyp)),
  assertz(prob_inferece),
  real_evaluation_aux(Queries, ProbValues),
  %%%%!, catch(
  %%%%(maplist:maplist(problog_exact, Queries, ProbValues, _)),
  %%%%error(_),
  %%%%fail),
  %!, catch(
    %problog_inference(problog_exact, Queries, ProbValues),
    %error(resource_error(_),_),
    %(
      %numbervars(Hyp, 0, _), 
      %writeln(Hyp),
      %NewN is N + 1,
      %set_value(eval_counter, NewN),
      %fail
    %)
  %),
  retractall(prob_inferece),
  retract_disjunction(Hyp),
  difference(ExpValues, ProbValues, Diff),
  predictive_accuracy(ExpValues, ProbValues, PAcc),
  root_mean_square_error(ExpValues, ProbValues, RMSE),
  threshold_accuracy(Examples, ProbValues, TAcc).
  
  %evaluation([(metabolism(A):-interaction(B,C,D),interaction(C,B,D),interaction(A,C,E))], X).
  
real_evaluation_aux([], []) :-!.
real_evaluation_aux([QueriesH | QueriesT], [ProbValueH | ProbValueT]) :-
  once(problog_exact(QueriesH, ProbValueH, Status)),
  Status \== timeout, !,
  real_evaluation_aux(QueriesT, ProbValueT).
real_evaluation_aux(_, _) :-
  get_value(timeout_counter, N),
  writeln(timeout(N)),
  NewN is N + 1,
  set_value(timeout_counter, NewN), !,
  fail.


%%%%%%%%%%%%%%%%%%%%%%%%%%%% METRICS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
% difference(+[ExpectedValues], +[ProbValues], -Difference)
% calculates the difference between the expected values and the predictive values of a hypotheses
% values greater than 0 mean the hypothesis is above the expected values
difference(ExpValues, ProbValues, Difference) :-
  difference_aux(ExpValues, ProbValues, ListOfDiffs),
  sum_list(ListOfDiffs, Difference).
  
difference_aux([], [], []) :-!.
difference_aux([ExpValue | ExpValues], [PValue | PValues], [Diff | Diffs]) :-
  Diff is PValue - ExpValue,
  difference_aux(ExpValues, PValues, Diffs).
  
% root_mean_square_error(+[ExpectedValues], +[ProbValues], -RMSE)
% calculates the root mean square error between expected values and values predicted by a hypothesis
% better hypotheses should have RMSE close to 0
root_mean_square_error(ExpValues, ProbValues, RMSE) :-
  root_mean_square_error_aux(ExpValues, ProbValues, NoExamples, SumSquareDiffs),
  RMSE is SumSquareDiffs/NoExamples.
  
root_mean_square_error_aux([], [], 0, 0) :- !.
root_mean_square_error_aux([ExpValue | ExpValuesT], [PValue | PValuesT], NoExamples, SumSquareDiffs):-
  Diff is ExpValue - PValue,
  DiffSq is Diff*Diff,
  root_mean_square_error_aux(ExpValuesT, PValuesT, OtherNoExamples, OtherDiffsSq),
  NoExamples is OtherNoExamples + 1,
  SumSquareDiffs is DiffSq + OtherDiffsSq.
  
  
% probabilistic_accuracy(+[ExpectedValues], +[ProbValues], -ProbabilisticAccuracy)
% calculates the probabilistic accuracy of a hypothesis wrt the expected values
% is equivalent to absolute average error
% needs to be 1-accuracy because we are minimizing 
probabilistic_accuracy(ExpValues, ProbValues, PAcc) :-
  prob_metrics(ExpValues, ProbValues, TP, TN, FP, FN),
  Accuracy is (TP + TN) / (TP + TN + FP + FN),
  PAcc is 1.00 - Accuracy.
  
prob_metrics(ExpValueList, ProbValueList, TP, TN, FP, FN) :-
  prob_metrics_aux(ExpValueList, ProbValueList, AllTP, AllTN, AllFP, AllFN),
  sum_list(AllTP, TP),
  sum_list(AllTN, TN),
  sum_list(AllFP, FP),
  sum_list(AllFN, FN).
  
prob_metrics_aux([], [], [], [], [], []) :-!.
prob_metrics_aux([ExpValue | ExpValueT], [ProbValue | ProbValueT], [ThisTP | OtherTP], [ThisTN | OtherTN], [ThisFP | OtherFP], [ThisFN | OtherFN]) :-
  min(ExpValue, ProbValue, ThisTP),
  ExpValueNeg is 1.00 - ExpValue,
  ProbValueNeg is 1.00 - ProbValue,
  min(ExpValueNeg, ProbValueNeg, ThisTN),
  FPAux is ExpValueNeg - ThisTN,
  max(0.00, FPAux, ThisFP),
  FNAux is ExpValue - ThisTP,
  max(0.00, FNAux, ThisFN),
  prob_metrics_aux(ExpValueT, ProbValueT, OtherTP, OtherTN, OtherFP, OtherFN).

% predictive_accuracy(+[ExpectedValues], +[ProbValues], PAcc)
% equivalent to probabilistic accuracy, but is calculated differently
predictive_accuracy(ExpValues, ProbValues, ProbCoverage) :-
  predictive_accuracy_aux(ExpValues, ProbValues, NoExamples, SumDiffs),
  ProbCoverage is SumDiffs/NoExamples.
  
predictive_accuracy_aux([], [], 0, 0) :- !.
predictive_accuracy_aux([ExpValue | ExpValuesT], [PValue | PValuesT], NoExamples, SumDiffs):-
  Diff is abs(ExpValue - PValue),
  predictive_accuracy_aux(ExpValuesT, PValuesT, OtherNoExamples, OtherDiffs),
  NoExamples is OtherNoExamples + 1,
  SumDiffs is Diff + OtherDiffs.
  
  
% threshold_accuracy(+[Queries], +[ProbValues], (+Threshold), -TAcc)
% classifies hypothesis prediction based on a threshold
% queries whose predicted value is greater than the threshold are considered positive; negative otherwise
% if no threshold is provided, uses 0.5
% useful to compare against discrete systems
threshold_accuracy(Examples, ProbValues, TAcc):-
  threshold_accuracy(Examples, ProbValues, 0.5, TAcc).
threshold_accuracy(Examples, ProbValues, Threshold, TAcc) :-
  threshold_accuracy_aux(Examples, ProbValues, Threshold, Sum, Acc),
  Accuracy is Acc/Sum,
  TAcc is 1-Accuracy.
  
threshold_accuracy_aux([], [], _, 0, 0) :- !.
threshold_accuracy_aux([prob_example(_, Weight, _) | QueryT], [PValue | PValuesT], Threshold, Sum, Acc) :-
  Weight > 0,
  PValue > Threshold, !,
  threshold_accuracy_aux(QueryT, PValuesT, Threshold, NewSum, NewAcc),
  Sum is NewSum + 1,
  Acc is NewAcc + 1.
threshold_accuracy_aux([prob_example(_, Weight, _) | QueryT], [_PValue | PValuesT], Threshold, Sum, Acc) :-
  Weight > 0, !,
  threshold_accuracy_aux(QueryT, PValuesT, Threshold, NewSum, NewAcc),
  Sum is NewSum + 1,
  Acc = NewAcc.
threshold_accuracy_aux([prob_example(_, _Weight, _) | QueryT], [PValue | PValuesT], Threshold, Sum, Acc) :-
  PValue =< Threshold, !,
  threshold_accuracy_aux(QueryT, PValuesT, Threshold, NewSum, NewAcc),
  Sum is NewSum + 1,
  Acc is NewAcc + 1.
threshold_accuracy_aux([prob_example(_, _Weight, _) | QueryT], [_PValue | PValuesT], Threshold, Sum, Acc) :-
  threshold_accuracy_aux(QueryT, PValuesT, Threshold, NewSum, NewAcc),
  Sum is NewSum + 1,
  Acc = NewAcc.
  
  

  
  
%%%%%%%%%%%%%%%%%%%%%%%%%% remote evaluation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

create_evaluation_process :-
  create_evaluation_process(5000, _). 
create_evaluation_process(Port, File) :-
  make_files_atom(File, '[', FileAtom),
  atomic_concat(['/home/jcr/jcr-svn/trunk/mainline/ARCH/yap -l /home/jcr/jcr-svn/trunk/SkILL/skill_slave.yap -g "evaluation_server(', Port, ',', FileAtom, ')" &'], Command),
  system(Command),
  loop_connect_to_server(Port, ThisSocket, 10),
  writeln('I have connected'),
  set_skill_option(socket, ThisSocket).
  
make_files_atom([FT], Aux, FileAtom) :-
  atomic_concat([Aux, '\'', FT, '\'', ']'], FileAtom), !.
make_files_atom([FH | FT], Aux, FileAtom) :-
  atomic_concat([Aux, '\'', FH, '\'', ','], NewAux),
  make_files_atom(FT, NewAux, FileAtom).
 
loop_connect_to_server(_, _, 0) :-
  writeln('CANT CONNECT TO SOCKET'),
  halt.
loop_connect_to_server(Port, ThisSocket, _N) :-
  sleep(0.5),
  jcr_connect_to_socket(Port, ThisSocket),
  nonvar(ThisSocket), !.
loop_connect_to_server(Port, ThisSocket, N) :-
  NewN is N-1,
  loop_connect_to_server(Port, ThisSocket, NewN).
  
socket_send(Socket, Hyp) :-
  %mutex_lock(temp_mutex_mpi),
  %term_to_atom(Hyp, HypAtom),
  %mutex_unlock(temp_mutex_mpi),
  Hyp = HypAtom,
  jcr_write_to_socket(Socket, HypAtom).
  
socket_recv(Socket, Term) :-
  Term = Atom,
  jcr_read_from_socket(Socket, Atom).
  %mutex_lock(temp_mutex_mpi),
  %atom_to_term(Atom, TermAux, _),
  %mutex_unlock(temp_mutex_mpi),
  %varnumbers(TermAux, Term).
  
  
%%%%%%%%%%%%%% aux %%%%%%%%%%%%%%%
  
  
assert_disjunction([]) :-!.
assert_disjunction([H | T]):-
  decompose_clause(H, _, _, _, Name, Arity),
  dynamic(Name/Arity),
  assertz(H),
  assert_disjunction(T).

retract_disjunction([]) :-!.
retract_disjunction([H|_T]) :-
  decompose_clause(H, _, _, _, Name, Arity),
  abolish(Name, Arity).
  
min(N1, N2, N2) :-
  N1 > N2, !.
min(N1, _N2, N1).

max(N1, N2, N1) :-
  N1 > N2, !.
max(_N1, N2, N2).