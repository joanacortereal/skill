:- consult('utils.yap').

%%%%%%%%%%%%%%%%%%%%%%%%%%% AND PRUNING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prune_search_space_rules(off, Population, Population).

prune_search_space_rules(soft, Population, PrunedPopulation) :-
  length(Population, LenPop),
  prune_search_space_aux_rules(soft, Population, PrunedPopulation),
  length(PrunedPopulation, LenPrunPop),
  writeln(pruned_search_space_rules(LenPrunPop/LenPop)).
  %LenPrunPop > 0.
  
prune_search_space_rules(hard, Population, PrunedPopulation) :-
  exp_values(ExpValues),
  prune_search_space_aux_rules(hard, Population, ExpValues, PrunedPopulation),
  length(Population, LenPop),
  length(PrunedPopulation, LenPrunPop),
  writeln(pruned_search_space_rules(LenPrunPop/LenPop)).
  %LenPrunPop > 0.
  
prune_search_space_aux_rules(soft, [], []) :-!.
prune_search_space_aux_rules(soft, [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | T], [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | OtherT]) :-
  Diff >= 0, !,
  prune_search_space_aux_rules(soft, T, OtherT).
prune_search_space_aux_rules(soft, [_H | T], Out) :-
  prune_search_space_aux_rules(soft, T, Out).

prune_search_space_aux_rules(hard, [], _, []) :-!.
prune_search_space_aux_rules(hard, [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | Rules], ExpValues, [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | PrunedPopulation]) :-
  good_rule_rules(hard, ProbValues, ExpValues), !,
  prune_search_space_aux_rules(hard, Rules, ExpValues, PrunedPopulation).
prune_search_space_aux_rules(hard, [_ | Rules], ExpValues, PrunedPopulation) :-
  prune_search_space_aux_rules(hard, Rules, ExpValues, PrunedPopulation).
  
%succeeds if rule should be kept
good_rule_rules(hard, [], []) :- !.
good_rule_rules(hard, [PValue | PValues], [ExpValue | ExpValues]) :-
  PValue >= ExpValue, !,
  good_rule_rules(hard, PValues, ExpValues).
  
%%% estimation pruning  

prune_rule_space_conjunction(off, _, _).

prune_rule_space_conjunction(hard, hyp(_Hyp, ProbValues, _Diff, _PAcc, _RMSE, _TAcc), hyp(_OtherHyp, OtherProbValues, _OtherDiff, _OtherPAcc, _OtherRMSE, _OtherTAcc)) :-
  exp_values(ExpValues),
  prune_rule_space_conjunction_aux(hard, ProbValues, OtherProbValues, ExpValues), !.

prune_rule_space_conjunction(soft, hyp(_Hyp, ProbValues, _Diff, _PAcc, _RMSE, _TAcc), hyp(_OtherHyp, OtherProbValues, _OtherDiff, _OtherPAcc, _OtherRMSE, _OtherTAcc)) :-
  exp_values(ExpValues),
  prune_rule_space_conjunction_aux(soft, ProbValues, OtherProbValues, ExpValues, DiffList),
  sum_list(DiffList, Sum),
  Sum >= 0.
  
prune_rule_space_conjunction_aux(soft, [], [], [], []) :-!.
prune_rule_space_conjunction_aux(soft, [PValue | PValues], [OtherValue | OtherValues], [ExpValue | ExpValues], [Diff | Diffs]) :-
  min(PValue, OtherValue, Max),
  Diff is Max - ExpValue,
  prune_rule_space_conjunction_aux(soft, PValues, OtherValues, ExpValues, Diffs).
  
prune_rule_space_conjunction_aux(hard, [], [], [], []) :-!.
prune_rule_space_conjunction_aux(hard, [PValue | PValues], [OtherValue | OtherValues], [ExpValue | ExpValues], [_Diff | Diffs]) :-
  min(PValue, OtherValue, Max),
  Max >= ExpValue, !,
  prune_rule_space_conjunction_aux(hard, PValues, OtherValues, ExpValues, Diffs).
  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%% POPULATION SELECTION %%%%%%%%%%%%%%%%%%%%%%%%%%
 
select_population(PrimaryRank, SecondaryRank, PrimaryPopulation, SecondaryPopulation, PrimarySize, SecondarySize, Primary, Secondary) :-
  length(PrimaryPopulation, LengthPrim),
  (
    PrimarySize > LengthPrim ->
    select_population_aux(PrimaryRank, PrimaryPopulation, LengthPrim, Primary)
  ;
    select_population_aux(PrimaryRank, PrimaryPopulation, PrimarySize, Primary)
  ),
  length(SecondaryPopulation, LengthSec),
  (
    SecondarySize > LengthSec ->
    select_population_aux(SecondaryRank, SecondaryPopulation, LengthSec, Secondary)
  ;
    select_population_aux(SecondaryRank, SecondaryPopulation, SecondarySize, Secondary)
  ).
  
select_population_aux(random, Population, Size, FinalMembers) :-
  n_best_rules_from_list(random, Population, Size, FinalMembers).
  
select_population_aux(pacc, Population, Size, FinalMembers) :-
  n_best_rules_from_list(pacc, Population, Size, FinalMembers).
 
select_population_aux(rmse, Population, Size, FinalMembers) :-
  n_best_rules_from_list(rmse, Population, Size, FinalMembers).
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PRUNING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prune_search_space(+Pruning, +[Population], -[PrunedPopulation])
% prunes search space of rules in Population using different pruning criteria
% options for Pruning are: off, soft, hard
% off: no pruning is performed
% soft: removes hypotheses overall uninteresting
% hard: removes hypotheses even slightly uninteresting
prune_search_space(off, Population, Population).

prune_search_space(soft, Population, PrunedPopulation) :-
  length(Population, LenPop),
  prune_search_space_aux(soft, Population, PrunedPopulation),
  length(PrunedPopulation, LenPrunPop),
  writeln(pruned_search_space(LenPrunPop/LenPop)).
  
prune_search_space(hard, Population, PrunedPopulation) :-
  exp_values(ExpValues),
  prune_search_space_aux(hard, Population, ExpValues, PrunedPopulation),
  length(Population, LenPop),
  length(PrunedPopulation, LenPrunPop),
  writeln(pruned_search_space(LenPrunPop/LenPop)).
  
prune_search_space_aux(soft, [], []) :-!.
prune_search_space_aux(soft, [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | T], [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | OtherT]) :-
  Diff =< 0, !,
  prune_search_space_aux(soft, T, OtherT).
prune_search_space_aux(soft, [_H | T], Out) :-
  prune_search_space_aux(soft, T, Out).

prune_search_space_aux(hard, [], _, []) :-!.
prune_search_space_aux(hard, [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | Rules], ExpValues, [hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc) | PrunedPopulation]) :-
  good_rule(hard, ProbValues, ExpValues), !,
  prune_search_space_aux(hard, Rules, ExpValues, PrunedPopulation).
prune_search_space_aux(hard, [_ | Rules], ExpValues, PrunedPopulation) :-
  prune_search_space_aux(hard, Rules, ExpValues, PrunedPopulation).
  
%succeeds if rule should be kept
good_rule(hard, [], []) :- !.
good_rule(hard, [PValue | PValues], [ExpValue | ExpValues]) :-
  PValue =< ExpValue, !,
  good_rule(hard, PValues, ExpValues).
  
  
% prune_rule_space(+Pruning, +Population, -PrunedPopulation)
% prunes combinations of hypotheses based on the values of the combining members
% same criteria as search space: off, soft and hard
prune_rule_space(off, _Rule, OtherRules, OtherRules) :-!.
  
prune_rule_space(soft, _, [], []) :-!.
prune_rule_space(soft, hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc), [hyp(OtherHyp, OtherProbValues, OtherDiff, OtherPAcc, OtherRMSE, OtherTAcc) | OtherRules], [hyp(OtherHyp, OtherProbValues, OtherDiff, OtherPAcc, OtherRMSE, OtherTAcc) | Rules]) :-
  exp_values(ExpValues), 
  prune_rule_space_1rule(soft, ProbValues, OtherProbValues, ExpValues, DiffList),
  sum_list(DiffList, Sum),
  Sum > 0, !,
  prune_rule_space(soft, hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc), OtherRules, Rules).
prune_rule_space(soft, Rule, [_OtherRule | OtherRules], ListOfRules) :-
  prune_rule_space(soft, Rule, OtherRules, ListOfRules).
  
prune_rule_space(hard, _, [], []) :-!.
prune_rule_space(hard, hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc), [hyp(OtherHyp, OtherProbValues, OtherDiff, OtherPAcc, OtherRMSE, OtherTAcc) | OtherRules], [hyp(OtherHyp, OtherProbValues, OtherDiff, OtherPAcc, OtherRMSE, OtherTAcc) | Rules]) :-
  exp_values(ExpValues), 
  good_combination(hard, ProbValues, OtherProbValues, ExpValues), !,
  prune_rule_space(hard, hyp(Hyp, ProbValues, Diff, PAcc, RMSE, TAcc), OtherRules, Rules).
prune_rule_space(hard, Rule, [_OtherRule | OtherRules], ListOfRules) :-
  prune_rule_space(hard, Rule, OtherRules, ListOfRules).
  
prune_rule_space_1rule(soft, [], [], [], []) :-!.
prune_rule_space_1rule(soft, [PValue | PValues], [OtherValue | OtherValues], [ExpValue | ExpValues], [Diff | Diffs]) :-
  MaxAux is PValue + OtherValue,
  max(PValue, OtherValue, Min),
  min(MaxAux, 1, Max),
  Contribution is (Max + Min)/2,
  Diff is Contribution - ExpValue,
  prune_rule_space_1rule(soft, PValues, OtherValues, ExpValues, Diffs).
  
% succeeds if rule should be kept
good_combination(hard, [], [], []) :-!.
good_combination(hard, [PValue | PValues], [OtherValue | OtherValues], [ExpValue | ExpValues]) :-
  MaxAux is PValue + OtherValue,
  min(MaxAux, 1, Max),
  Max >= ExpValue, !,
  good_combination(hard, PValues, OtherValues, ExpValues).
 